import 'dart:io';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter/material.dart';

class Objekt   {
  String id,id_user,name_user="null",name,category,date,plz,strasse,stadt,image_name,description="ok____";
  int key, price,tel_nummer,action;
  File image_object;


  Objekt({
    this.id,
    this.id_user,
    this.name_user,
    this.name,
    this.action,
    this.category,
    this.description,
    this.price,
    this.image_object,
    this.tel_nummer,
    this.plz,
    this.stadt,
    this.strasse,
    this.date,
    this.image_name
  });

  Objekt.fromMap(Map<dynamic,dynamic> data){
    name= data['name'];
    id_user=data['id_user'] ;
    name_user=data['name_user'] ;
    action= data['action'];
    category=data['category'];
    description= data['description'];
    price= data['price'];
    image_object=data['image_object'];
    tel_nummer= data['tel_nummer'];
    plz= data['plz'];
    stadt= data['stadt'];
    strasse= data['strasse'];
    image_name= data['image_name'];
    date=data['date'];
  }

}
