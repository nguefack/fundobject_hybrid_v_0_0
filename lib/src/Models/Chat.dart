import 'package:firebase_database/firebase_database.dart';
import  'package:flutter/material.dart';
import 'package:fund_app/src/Models/Objekt.dart';
import 'package:fund_app/src/repository/objekt_repository.dart';

import 'package:firebase_database/firebase_database.dart';
import  'package:flutter/material.dart';
import 'package:fund_app/src/Models/Objekt.dart';
import 'package:fund_app/src/repository/objekt_repository.dart';


class Chat {
  final String chat_table="chat";
  String id_sender="null";
  String name_sender="null";
  String id_receiver="null";
  String name_receiver="null";
  String message="null";
  var time;
  String date="null";

  Chat({this.id_sender, this.message, this.id_receiver,this.time});


  Future<bool> save_chat(Chat chat){
    DatabaseReference dR = FirebaseDatabase.instance.reference().child(chat_table);
    toJson(){
      return{
        "id_sender":chat.id_sender,
        "name_sender":chat.name_sender,
        "id_receiver":chat.id_receiver,
        "name_receiver":chat.name_receiver,
        "message":chat.message,
        "time":chat.time,
        "date":chat.date
      };
    }
    dR.push().set(toJson());
  }
  Future<Query> get_ChatList() async{

    return FirebaseDatabase.instance.reference().child(chat_table);

  }

}


class Chat_tmp {
  String id_customer;
  String name;
  String time;
  String message;
  String profil;

  Chat_tmp({this.id_customer, this.name, this.message, this.time, this.profil});

  verified_list(List<Chat_tmp> list, Chat_tmp _chat_tmp){
    bool exist=false;
    for(Chat_tmp chat_tmp_ in list){
      if(chat_tmp_.id_customer==_chat_tmp.id_customer){
        list.remove(chat_tmp_);
        exist= true;
        break;
      }
    }

  }
}























/*

class Chat {
  final String chat_table = "chat";
  String id_sender = "null";
  String name_sender = "null";
  String id_receiver = "null";
  String name_receiver = "null";
  String message = "null";
  var time;
  String date = "null";

  Chat({this.id_sender,this.name_sender, this.message, this.id_receiver,this.name_receiver, this.time});


}


class Chat_tmp {
    String id_customer;
    String name;
    String time;
    String message;
    String profil;

  Chat_tmp({this.id_customer, this.name, this.message, this.time, this.profil});

    verified_list(List<Chat_tmp> list, Chat_tmp _chat_tmp){
          bool exist=false;
          for(Chat_tmp chat_tmp_ in list){
            if(chat_tmp_.id_customer==_chat_tmp.id_customer){
              list.remove(chat_tmp_);
                exist= true;
                break;
            }
          }

  }
}

* */