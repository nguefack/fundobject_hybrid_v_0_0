/*
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:fund_app/src/Models/Chat.dart';
import 'package:fund_app/src/Models/User.dart';
import 'package:fund_app/src/repository/chat_repository.dart';
import 'package:fund_app/src/repository/login_repository.dart';
import 'package:fund_app/src/screens/Home_user/homeUser.dart';
import 'package:fund_app/src/screens/widgets/themes_colors.dart';

class ChatWindow extends StatefulWidget {
  String  id_receiver;
  ChatWindow({Key key, this.id_receiver}):super(key :key);
  @override
  _ChatWindowState createState() => _ChatWindowState();
}

class _ChatWindowState extends State<ChatWindow> {
  List<Chat> _messages_list,_messages_list_sort;
  String profil= "https://tutorialandroid2017.files.wordpress.com/2017/04/android-1.jpg?w=1519&zoom=2";
  String profil_sender="https://images.pexels.com/photos/7117/mountains-night-clouds-lake.jpg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940",id_user, title="F";
  Color_app color_app= new Color_app();
  TextEditingController textEditingController;
  ScrollController scrollController;
  UserServices p = new UserServices();
  bool enableButton = false;
  Chat chat= new Chat();
  ChatRepository chatRepository ;

  @override
  void initState() {

    p.getId_().then((onValue) {
      if (onValue != null) {
        setState(() {
          id_user=onValue;
        });
      }
    });
    _messages_list= List<Chat>();
    _messages_list_sort= List<Chat>();

    chatRepository.get_ChatList().then((Query query){
      query.once().then((DataSnapshot snap){
        var keys= snap.value.keys;
        var data= snap.value;
        for(var key in keys){
          if(data[key]['id_sender']==id_user && data[key]['id_receiver']==widget.id_receiver || data[key]['id_receiver']==id_user && data[key]['id_sender']==widget.id_receiver ){
            Chat chat= new Chat(
              id_sender: data[key]['id_sender'],
              id_receiver: data[key]['id_receiver'],
              message: data[key]['message'],
              time: data[key]['time']

            );

            setState(() {
              this._messages_list_sort.add(chat);
             });
          }
            print(data[key]['message']+"\n");
        }

      });
    });

    textEditingController = TextEditingController();
    scrollController = ScrollController();

    super.initState();
  }


  void handleSendMessage() {
    var text = textEditingController.value.text;
    textEditingController.clear();

    UserServices p =new UserServices();
    p.getId_().then((onValue){
      if(onValue!=null) {
          Chat chat= new Chat();
          chat.id_sender=onValue;
          chat.id_receiver= widget.id_receiver;
          chat.message=text;
          chat.time= formattime(DateTime.now());
          chat.date=formatDate(DateTime.now());
          chatRepository.save_chat(chat);
          setState(() {
              _messages_list.add(chat);
            enableButton = false;
          });

      }else{
        print("error user must first login");
      }
    });


    Future.delayed(Duration(milliseconds: 100), () {
      scrollController.animateTo(scrollController.position.maxScrollExtent,
          curve: Curves.ease, duration: Duration(milliseconds: 500));
    });

  }

  @override
  Widget build(BuildContext context) {
     int  i,j;
     Chat chat_= new Chat();
    for(i= 0 ; i<_messages_list_sort.length;i++){

      chat_=_messages_list_sort[i];

      for(j=1;j<_messages_list_sort.length-i;j++ ){
        if(_messages_list_sort[j-1].time>_messages_list_sort[j].time){
          chat_= _messages_list_sort[j-1];
          _messages_list_sort[j-1]=_messages_list_sort[j];
          _messages_list_sort[j]=chat_;
        }

      }

    }
    setState(() {
       this._messages_list=_messages_list_sort;
    });
    var textInput = Row(
      children: <Widget>[
        Expanded(
          child: Padding(
            padding: const EdgeInsets.only(left: 8.0),
            child: TextField(
              onChanged: (text) {
                setState(() {
                  enableButton = text.isNotEmpty;
                });
              },
              decoration: InputDecoration.collapsed(
                hintText: "Type a message",
              ),
              controller: textEditingController,
            ),
          ),
        ),
        enableButton
            ? IconButton(
                color: Theme.of(context).primaryColor,
                icon: Icon(
                  Icons.send,
                ),
                disabledColor: Colors.grey,
                onPressed: handleSendMessage,
              )
            : IconButton(
                color: Colors.blue,
                icon: Icon(
                  Icons.send,
                ),
                disabledColor: Colors.grey,
                onPressed: null,
              )
      ],
    );

    return Scaffold(

      backgroundColor: color_app.color_app_grey,
      resizeToAvoidBottomPadding: true,
      appBar: new AppBar(
        elevation: 0.0,
        title: Text('message'),
        leading: IconButton(icon: Icon(Icons.arrow_back),
            onPressed:() =>Navigator.of(context).push(new MaterialPageRoute(
                builder: (BuildContext context) => new HomeUser()))
        ),
      ),
      body: Column(

        children: <Widget>[
          Expanded(
            child: ListView.builder(

              controller: scrollController,
              itemCount: _messages_list.length,
              itemBuilder: (context, index) {

                 Widget message;
                if (_messages_list[index].id_receiver == widget.id_receiver) {
                  var messagebody = DecoratedBox(
                    decoration: BoxDecoration(
                      color: color_app.color_app_bagrchat,
                      borderRadius: BorderRadius.circular(8.0),
                    ),
                    child: Align(
                      alignment: Alignment.centerLeft,
                      child: Padding(
                        padding: const EdgeInsets.all(12.0),
                        child: Text(_messages_list[index].message),
                      ),
                    ),
                  );
                  var triangle = CustomPaint(
                    painter: Triangle(color_app:this.color_app.color_app_bagrchat ),
                  );
                    var avatar = Padding(
                    padding:
                    const EdgeInsets.only(left: 8.0, bottom: 8.0, right: 8.0),
                    child: CircleAvatar(
                      child: new CircleAvatar(
                        foregroundColor: color_app.color_app_bagrchat,
                        backgroundColor: color_app.color_app_bagrchat,
                        backgroundImage: new NetworkImage(this.profil),

                      ),
                    ),
                  );
                  message = Stack(
                    children: <Widget>[
                      messagebody,
                      Positioned(right: 0, bottom: 0, child: triangle),
                    ],
                  );
                  return Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: message,
                      ),
                      avatar,
                    ],
                  );
                }else{
                  var messagebody = DecoratedBox(
                    decoration: BoxDecoration(
                      color: color_app.color_app_white,
                      borderRadius: BorderRadius.circular(8.0),
                    ),
                    child: Align(
                      alignment: Alignment.centerLeft,
                      child: Padding(
                        padding: const EdgeInsets.all(12.0),
                        child: Text(_messages_list[index].message),
                      ),
                    ),
                  );
                  var triangle = CustomPaint(
                    painter: Triangle(color_app:this.color_app.color_app_white ),
                  );
                   var avatar = Padding(
                    padding:
                    const EdgeInsets.only(left: 8.0, bottom: 8.0, right: 8.0),
                    child: CircleAvatar(
                      child:CircleAvatar(
                        child: new CircleAvatar(
                          foregroundColor: color_app.color_app_bagrchat,
                          backgroundColor: color_app.color_app_bagrchat,
                          backgroundImage: new NetworkImage(this.profil_sender),

                        ),
                      ),
                    ),
                  );
                  message = Stack(
                    children: <Widget>[
                      Positioned(left: 0, bottom: 0, child: triangle),
                      messagebody,
                    ],
                  );
                  return Row(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: <Widget>[
                      avatar,
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: message,
                      ),
                    ],
                  );
                }
              },
            ),
          ),
          Divider(height: 2.0),
          textInput
        ],
      ),
    );
  }
  String formatDate(DateTime d) {
    var month = d.month;
    var day = d.day;
    var year = d.year;
    return "$day/$month/$year";
  }
   formattime(DateTime d) {
    var time=d.toUtc().millisecondsSinceEpoch;

    return time;
  }
}

class Triangle extends CustomPainter {
  Color color_app;
  Triangle({Key key,this.color_app});


  @override
  void paint(Canvas canvas, Size size) {
    var paint = Paint()..color = this.color_app;

    var path = Path();
    path.lineTo(10, 0);
    path.lineTo(0, -10);
    path.lineTo(-10, 0);
    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) {
    return true;
  }

}


* */