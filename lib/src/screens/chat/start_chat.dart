import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:fund_app/src/screens/chat/chat_window.dart';
import 'package:fund_app/src/screens/widgets/themes_colors.dart';



void main() => runApp(ChatWidget());

class ChatWidget extends StatelessWidget {
  @override
  String   id_receiver,name_receiver;

  ChatWidget({Key key, this.id_receiver,this.name_receiver}):super(key: key);
  Widget build(BuildContext context) {

    return MyHomePage(id_receiver:id_receiver,name_receiver:name_receiver);
  }
}

class MyHomePage extends StatefulWidget {
  String id_receiver,name_receiver;
  MyHomePage({Key key, this.title,this.id_receiver,this.name_receiver}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  Color_app color_app= new Color_app();
  @override
  build(context){
    return MaterialApp(
      theme: new ThemeData(primarySwatch: Colors.lime,
          primaryColor: defaultTargetPlatform ==
              TargetPlatform.iOS ? color_app.color_app_def_val
              : color_app.color_app_def_val),
      debugShowCheckedModeBanner: false,
      home: ChatWindow(id_receiver:widget.id_receiver,name_receiver:widget.name_receiver),
    );
  }
}
