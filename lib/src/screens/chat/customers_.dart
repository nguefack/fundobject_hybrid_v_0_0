/*
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:fund_app/src/Models/Chat.dart';
import 'package:fund_app/src/Models/Objekt.dart';
import 'package:fund_app/src/Models/User.dart';
import 'package:fund_app/src/repository/chat_repository.dart';
import 'package:fund_app/src/repository/login_repository.dart';
import 'package:fund_app/src/screens/chat/start_chat.dart';


class Customers extends StatefulWidget {

  List<Objekt> my_objects;
  Customers({Key  key,this.my_objects}):super(key:key);


  @override
 Customers_body createState() => new Customers_body();
}
class Customers_body extends State<Customers>{
  UserServices p = new UserServices();
  String profil= "https://tutorialandroid2017.files.wordpress.com/2017/04/android-1.jpg?w=1519&zoom=2";
  List<Chat_tmp> ListeCustomers=[];
  String  id_user;
  Chat chat= new Chat();
  ChatRepository chatRepository ;

  @override
  void initState() {


    ListeCustomers= List<Chat_tmp>();
    p.getId_().then((onValue) {
      if (onValue != null) {
        setState(() {
          id_user=onValue;
        });
      }
    });

    chatRepository.get_ChatList().then((Query query){
      query.once().then((DataSnapshot snap){
        var keys= snap.value.keys;
        var data= snap.value;
        for(var key in keys){
          if(data[key]['id_sender']==id_user  || data[key]['id_receiver']==id_user ){
            Chat_tmp chat_tmp= new Chat_tmp();
            if(data[key]['id_sender']==id_user){

              chat_tmp.profil=this.profil;//get_profil_user(data[key]['id_receiver']);
              chat_tmp.name=data[key]['name_receiver'];//get_name_user(data[key]['id_receiver']); this value must be change width the real value of the user
              chat_tmp.id_sender=data[key]['id_receiver'];
              chat_tmp.time=data[key]['date'];
            }else {
              chat_tmp.profil=this.profil;//get_profil_user(data[key]['id_sender']);
              chat_tmp.name=data[key]['name_sender'];//get_name_user(data[key]['id_sender']); this value must be change width the real value of the user
              chat_tmp.id_sender=data[key]['id_sender'];

            }
            chat_tmp.message=data[key]['message'];
            chat_tmp.time=data[key]['date'].toString();
            setState(() {

              chat_tmp.verified_list(this.ListeCustomers,chat_tmp);
              this.ListeCustomers.add(chat_tmp);
            });

          }else{
            print("print error");
          }



        }

      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    return  ListView.builder(
          itemCount:ListeCustomers.length ,
          itemBuilder: (context,i)=> new Column(
            children: <Widget>[

              new Divider(

                height: 10.0,
              ),
              GestureDetector(
                child: new ListTile(
                  onTap: ()=> redirect(ListeCustomers[i].id_sender),
                  leading: new CircleAvatar(
                  foregroundColor: Colors.indigo,
                    backgroundColor: Colors.cyan,
                    backgroundImage: new NetworkImage(ListeCustomers[i].profil),

                  ),
                  title: new Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      new Text(ListeCustomers[i].name, style: new TextStyle(fontWeight: FontWeight.bold)),
                      new Text(ListeCustomers[i].time, style: new TextStyle(color: Colors.cyan,fontSize:14.0)),

                    ],
                  ),
                  subtitle: new Container(
                    padding: const EdgeInsets.only(top: 5.0),
                    child: new Text(ListeCustomers[i].message, style: new TextStyle(color: Colors.grey,fontSize:15.0)),

                  ),

                ),
              )
            ],
          ),
    );
  }
  void redirect(String id_receiver){

    Navigator.of(context).push(new MaterialPageRoute(
        builder: (BuildContext context) => new ChatWidget(id_receiver:id_receiver )));
  }
}
* */