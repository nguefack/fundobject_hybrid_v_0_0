import 'package:flutter/material.dart';
import 'package:fund_app/src/Models/Objekt.dart';
import 'package:fund_app/src/screens/objekt_list/object-detail_view.dart';
import 'package:fund_app/src/screens/widgets/image_ui.dart';

class single_object extends StatelessWidget {

  final object_name;
  final object_picture;
  final object_description;
  final object_stadt;

  final Objekt object_;
  single_object(
  {
    this.object_,
    this.object_name,
    this.object_stadt,
    this.object_description,
    this.object_picture

}
      );


  @override
  Widget build(BuildContext context) {
    return Card(
      child: Hero(
          tag: object_.name,
          child: Material(
            child: InkWell(
              onTap: () => Navigator.of(context).push(MaterialPageRoute(
                builder: (context) => objectDetailView(
                  objekt:object_
                )
              )),
              child: GridTile(
                footer: Container(
                  color: Colors.white70,
                  child: ListTile(

                    title: Center(
                      child: Text(
                        object_.name,
                        style: TextStyle(
                            fontWeight: FontWeight.w800, fontSize: 16.0),
                      ),
                    ),
                  ),
                ),
                  child:  new Image_url(image_url_:object_.image_name)
              ),
            ),
          )
      ),
    );
  }
}


