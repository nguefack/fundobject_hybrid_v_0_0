import 'package:flutter/material.dart';
import 'package:fund_app/src/Models/Filter.dart';

class CategoryFilterUI extends StatefulWidget {
  @override
  _CategoryFilterUIState createState() => _CategoryFilterUIState();
}

class _CategoryFilterUIState extends State<CategoryFilterUI> {
  final List<CategoryFilter> _categoryList = <CategoryFilter>[
    const CategoryFilter("lost item"),
    const CategoryFilter("rental item "),
    //const CategoryFilter("Supplies and Materials"),
    //const CategoryFilter("Other")
  ];

    List<String> _filters = <String>[];

    Iterable<Widget> get categoriesWidget sync* {
      for(CategoryFilter category in _categoryList){
        yield Padding(
          padding: const EdgeInsets.all(4.0),
          child:     FilterChip(
            label: Text(category.name),
            backgroundColor: Colors.grey[200],
            selected: _filters.contains(category.name),
            onSelected: (bool value) {print("selected");},
          ),
        );
      }
    }
  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
          scrollDirection: Axis.horizontal,
      padding: const EdgeInsets.fromLTRB(10.0, 10.0, 10.0, 0.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: categoriesWidget.toList(),
      ),
    );
  }
}
