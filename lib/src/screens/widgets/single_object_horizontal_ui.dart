import 'package:flutter/material.dart';
import 'single_object_horizontal.dart';

class SingleObjectHorizontalUi extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 140.0,
      child: ListView(
        scrollDirection: Axis.horizontal,
        children: <Widget>[
          SingleObjectHorizontal(image_location:'images/car_keys.jpg', image_caption: 'Car keys'),
          SingleObjectHorizontal(image_location:'images/iphone7.jpg', image_caption: 'Iphone 7'),
          SingleObjectHorizontal(image_location:'images/phone_charger.jpg', image_caption: 'Phone Charger'),
          SingleObjectHorizontal(image_location:'images/ring.jpg', image_caption: 'Ring'),
          SingleObjectHorizontal(image_location:'images/usb_stick.jpg', image_caption: 'USB stick'),
        ],
      ),
    );
  }
}
