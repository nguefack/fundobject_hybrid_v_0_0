import 'package:flutter/material.dart';
import 'dart:math';

import 'package:fund_app/src/screens/widgets/themes_colors.dart';
class Loading extends StatefulWidget{
  @override
  _Loader createState() => _Loader();

}

class _Loader extends State<Loading> with SingleTickerProviderStateMixin{
  AnimationController controller;
  Animation<double> animation_rotation;
  Animation<double> animation_raduis_in;
  Animation<double> animation_raduis_out;
  Color_app color_app= new Color_app();

  final double initial_radius= 60.0;

  double radius=0.0;

  @override
  void initState(){
   super.initState();

   controller= AnimationController(vsync: this,duration: Duration(seconds: 5));

   animation_rotation=Tween<double>(
      begin:0.0,
      end:1.0,
    ).animate(CurvedAnimation(parent: controller, curve: Interval(0.0,1.0,curve: Curves.linear)));

   animation_raduis_in= Tween<double>(
       begin:1.0,
      end:0.0,
    ).animate(CurvedAnimation(parent: controller, curve: Interval(0.75,1.0,curve: Curves.elasticIn),));

   animation_raduis_out= Tween<double>(
     begin:0.0,
     end:1.0,
   ).animate(CurvedAnimation(parent: controller, curve: Interval(0.0,0.25,curve: Curves.elasticOut),));

   controller.addListener((){
     setState(() {
       if(controller.value>=0.75 && controller.value<=1.0){
         radius=animation_raduis_in.value*initial_radius;
       }else if(controller.value>=0.0 && controller.value<=0.25){
         radius=animation_raduis_out.value*initial_radius;
       }
     });
    });
    controller.repeat();
  }


  @override
  Widget build(BuildContext context){
    return Center(
       // width: 100.0,
        //height: 100.0,
        child: Center(
          child: RotationTransition(
              turns: animation_rotation,
              child: Stack(
              children: <Widget>[
                Movement(
                  radius: 30.0,
                  color: color_app.color_app_def_val,
                ),
                Transform.translate(
                  offset: Offset( radius*cos(pi/4),radius*sin(pi/4)),
                  child: Movement(
                      radius: 5.0,
                      color:Colors.redAccent
                  ),
                ),
                Transform.translate(
                  offset: Offset( radius*cos(2*pi/4),radius*sin(2*pi/4)),
                  child: Movement(
                      radius: 5.0,
                      color:color_app.color_app_def_val
                  ),
                ),
                Transform.translate(
                  offset: Offset( radius*cos(3*pi/4),radius*sin(3*pi/4)),
                  child: Movement(
                      radius: 5.0,
                      color:Colors.redAccent
                  ),
                ),
                Transform.translate(
                  offset: Offset( radius*cos(4*pi/4),radius*sin(4*pi/4)),
                  child: Movement(
                      radius: 5.0,
                      color:color_app.color_app_def_val
                  ),
                ),
                Transform.translate(
                  offset: Offset( radius*cos(5*pi/4),radius*sin(5*pi/4)),
                  child: Movement(
                      radius: 5.0,
                      color:Colors.redAccent
                  ),
                ) ,
                Transform.translate(
                  offset: Offset( radius*cos(6*pi/4),radius*sin(6*pi/4)),
                  child: Movement(
                      radius: 5.0,
                      color:color_app.color_app_def_val
                  ),
                ),
                Transform.translate(
                  offset: Offset( radius*cos(7*pi/4),radius*sin(7*pi/4)),
                  child: Movement(
                      radius: 5.0,
                      color:Colors.redAccent
                  ),
                ),
                Transform.translate(
                  offset: Offset( radius*cos(8*pi/4),radius*sin(8*pi/4)),
                  child: Movement(
                      radius: 5.0,
                      color:color_app.color_app_def_val
                  ),
                )
              ],
          ),

          ),
      ),
    );
  }
}
class Movement extends StatelessWidget{
  final double radius;
  final Color color;
  Movement({this.radius,this.color});
  @override
  Widget build(BuildContext context){
    return Center(
      child: Container(
        width: this.radius,
        height: this.radius,
        decoration: BoxDecoration(
            color: this.color,
            shape: BoxShape.circle
        ),
      ) ,
    );
  }
}