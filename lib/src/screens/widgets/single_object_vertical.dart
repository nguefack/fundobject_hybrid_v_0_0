import 'package:flutter/material.dart';
import 'package:fund_app/src/Models/Objekt.dart';
import 'package:fund_app/src/repository/login_repository.dart';
import 'package:fund_app/src/screens/login_sign_up/login.dart';
import 'package:fund_app/src/screens/objekt_list/object-detail_view.dart';
import 'package:fund_app/src/screens/objekt_list/objekt_list_view_ausleihen.dart';

import 'image_ui.dart';

class singleObjectViewVertical extends StatelessWidget {
  final Objekt object_;
  String id_user_;
  singleObjectViewVertical({this.object_,this.id_user_});

  @override
  Widget build(BuildContext context) {
    return Container(
      //height: 331.0,
      child: Column(
        children: <Widget>[
          ListTile(
//        leading: Image.asset(
//          object_picture,
//          width: 80.0,
//          height: 80.0,
//        ),
            leading: CircleAvatar(
              backgroundColor: Colors.pinkAccent[100],
              child: Image.asset(
                'images/splash_image.png',
                fit: BoxFit.cover,
                height: 150.0,
              ),
            ),
            title: Text(object_.name),
            trailing: Icon(Icons.more_horiz),
          ),
          Hero(
            tag: object_.name,
            child: Material(
              child: InkWell(
                onTap: () => Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => objectDetailView(objekt: object_))),
                child: ListTile(
                  title: new Image_url(image_url_: object_.image_name),
                  /*
              title: Image.asset(
                object_picture,
                fit: BoxFit.cover,
                height: 150.0,
              ),
              */
                ),
              ),
            ),
          ),
          Row(children: <Widget>[
            Padding(
              padding: const EdgeInsets.only(left: 8.0),
              child: Stack(
                children: <Widget>[
                  Align(
                      alignment: Alignment.topRight,
                      child: IconButton(
                        icon: Icon(Icons.call, color: Colors.cyan),
                        onPressed: () => Navigator.of(context).push(
                            MaterialPageRoute(
                                builder: (context) =>
                                    objectDetailView(objekt: object_))),
                      )),
                ],
              ),
            ),
            Stack(
              children: <Widget>[
                Align(
                    alignment: Alignment.centerLeft,
                    child: IconButton(
                      icon: Icon(Icons.message, color: Colors.cyan),
                      onPressed: () => Navigator.of(context).push(
                          MaterialPageRoute(
                              builder: (context) =>
                                  objectDetailView(objekt: object_))),
                    )),
              ],
            ),

            Padding(
              padding: const EdgeInsets.only(left: 200.0),
              child: Stack(
                children: <Widget>[
                  Align(
                      alignment: Alignment.centerLeft,
                      child: IconButton(
                        icon: Icon(Icons.favorite_border, color: Colors.cyan),
                        onPressed: () => add_to_favorite(this.object_,context),
                        )),
                ],
              ),
            )
          ]),
          Padding(
            padding: const EdgeInsets.fromLTRB(20.0, 8.0, 8.0, 10.0),
            child: Container(
              alignment: Alignment.topLeft,
              child: Text(
                object_.description,
                overflow: TextOverflow.ellipsis,
                maxLines: 3,
              ),
            ),
          ),
        ],
      ),
    );
  }
  void add_to_favorite(Objekt objekt, context){

    if(id_user_!=null){
      if(id_user_!=objekt.id_user){
        Navigator.of(context).push(new MaterialPageRoute(
            builder: (BuildContext context) => new objektListViewAusleihen(redirect_to:1,object_:objekt)));
      }else{

      }
    }else{
      Navigator.of(context).push(new MaterialPageRoute(
          builder: (BuildContext context) => new  loginPage()));
    }
  }
}
