import 'package:flutter/material.dart';

class SingleObjectHorizontal extends StatelessWidget {
  
  final String image_location;
  final String image_caption;
  
  SingleObjectHorizontal({this.image_location, this.image_caption});
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(2.0),
      child: Container(
        width: 130.0,
        child: InkWell(
          onTap: (){},
          child: ListTile(
            title: Padding(
              padding: const EdgeInsets.only(bottom: 6.0),
              child: Container(
                decoration: BoxDecoration(
                    color: Colors.cyan[50],
                    borderRadius: BorderRadius.circular(12.0)
                ),
                child: Image.asset(image_location,
                width: 100.0,
                height: 100.0,),
              ),
            ),
            subtitle: Container(
              alignment: Alignment.topCenter,
              child: Text(image_caption, style: TextStyle(fontSize: 12.0),),
            ),
          ),
        ),
      ),
    );
  }
}

