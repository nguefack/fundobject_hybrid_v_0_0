import 'package:flutter/material.dart';

class Image_url extends StatelessWidget{
  final String image_url_;
  final String default_image="images/splash_image.png";
  Image_url({this.image_url_});

  @override
  Widget build(BuildContext context) {

    if(image_url_==null){
      return new Image.asset(default_image, fit: BoxFit.cover, height: 150.0,);
    }else{
      return  new Image.network(image_url_, fit: BoxFit.cover,height: 150.0,);
    }

  }
}