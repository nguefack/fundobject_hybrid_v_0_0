import 'dart:convert';

import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fund_app/src/Models/Objekt.dart';
import 'package:fund_app/src/repository/objekt_repository.dart';
import 'package:fund_app/src/Models/User.dart';
import 'package:fund_app/src/repository/login_repository.dart';
import 'package:fund_app/src/screens/Home_user/homeUser.dart';
import 'package:fund_app/src/screens/login_sign_up/signinupevent.dart';
import 'package:fund_app/src/screens/login_sign_up/singinupbloc.dart';
import 'package:fund_app/src/screens/login_sign_up/singinupstate.dart';

enum FormMode { LOGIN, SIGNUP }


class loginPage extends StatefulWidget {


  @override
  _loginPageState createState() => _loginPageState();
}

class _loginPageState extends State<loginPage> {

   Registration registration;
  final FirebaseAuth firebaseAuth = FirebaseAuth.instance;
  UserServices _userService = UserServices();
  final _formKey = new GlobalKey<FormState>();
   Rufe_Objekten rufe_objekten=new Rufe_Objekten();

  TextEditingController _telefonController = TextEditingController();
  TextEditingController _userNameController = TextEditingController();
  TextEditingController _emailTextController = TextEditingController();
  TextEditingController _passwordTextController = TextEditingController();
  TextEditingController _confirmPasswordTextController = TextEditingController();

  bool _isLoading = true;
  bool _hidePass = true;
  List<Objekt> ObjektList=[];
  FormMode _formMode = FormMode.LOGIN;
   User user_ = new User();

  @override
  void initState() {
    getListeObjekts();
    super.initState();
  }

   void getListeObjekts()async {
     var list= await rufe_objekten.get_ObjectList();
     setState(() {
       this.ObjektList= list;
     });

   }
  bool _validateAndSave(){
    final form = _formKey.currentState;
    if(form.validate()){
      form.save();
      return true;
    }
    return false;
  }

  bool validateAndSubmit()  {
    if(_validateAndSave()){
        if(_formMode != FormMode.LOGIN){

         setState(() {
              user_.userName = _userNameController.text;
              user_.telefonNummer = _telefonController.text;
              user_.email = _emailTextController.text;
              user_.password = _passwordTextController.text;
            });
        }
     //   Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => HomeUser()));
      return true;
    }
    return false;
  }

  void _changeFormToSignUp() {
    _formKey.currentState.reset();
    setState(() {
      _formMode = FormMode.SIGNUP;
    });
  }

  void _changeFormToLogin() {
    _formKey.currentState.reset();
    setState(() {
      _formMode = FormMode.LOGIN;
    });
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      body: BlocProvider(
builder: (context)=>Loginbloc(Login(),Registration()),
        child: Stack(
          children: <Widget>[
            Image.asset(
              'images/login_background2.jpg',
              fit: BoxFit.fill,
              width: double.infinity,
              height: double.infinity,
            ),
//Include the Logo
            Container(
              color: Colors.black.withOpacity(0.4),
              width: double.infinity,
              height: double.infinity,
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(0.0, 50.0, 0.0, 0.0),
              child: Container(
                  alignment: Alignment.topCenter,
                  child: Image.asset('images/splash_image.png', width: 280.0, height: 240.0,)),
            ),

            BlocBuilder<Loginbloc,UserState>(
              builder:(context,state){
                if(state is UserIsNotConnected){
                  final signinupbloc=BlocProvider.of<Loginbloc>(context);
                  return Center(
                    child: Padding(
                      padding: const EdgeInsets.only(top: 250.0),
                      child: Center(
                        child: Form(
                            key: _formKey,
                            child: ListView(
                              children: buildInputs()
                                 + buildButtons(signinupbloc,context,_emailTextController.text,_passwordTextController.text)
                            )),
                      ),
                    ),
                  );
                }else if(state is UserIsLoading){

                  return Center(child : CircularProgressIndicator());
                }else if(state is UserIsConnected){

                  return HomeUser();

                }else if(state is UserIsNotLoaded){

                  return Center(child : Text(state.getMessage_error, style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: 20.0),));


                }else if(state is UserIsRegisted){

                  return HomeUser();
                }
                return Text("error welcon");
              },
            ),

          ],
        ),
      )
    );

  }







  List<Widget> buildInputs(){
    if(_formMode == FormMode.LOGIN){
      return [
        Padding(
          padding: const EdgeInsets.fromLTRB(20.0, 20.0, 20.0, 0.0),
          child: Material(
            borderRadius: BorderRadius.circular(10.0),
            color: Colors.white.withOpacity(0.8),
            elevation: 0.0,
            child: Padding(
              padding: const EdgeInsets.only(left: 12.0),
              child: TextFormField(
                controller: _emailTextController,
                decoration: InputDecoration(
                    hintText: "Email",
                    icon: Icon(Icons.alternate_email),
                    border: InputBorder.none
                  //border: OutlineInputBorder(),
                ),
                validator: (value){
                  if(value.isEmpty){
                    Pattern pattern =
                        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
                    RegExp regex = new RegExp(pattern);
                    if(!regex.hasMatch(value))
                      return 'Please enter a valid email address';
                    else
                      return null;
                  }
                },
              ),
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.fromLTRB(20.0, 20.0, 20.0, 0.0),
          child: Material(
            borderRadius: BorderRadius.circular(10.0),
            color: Colors.white.withOpacity(0.8),
            elevation: 0.0,
            child: Padding(
              padding: const EdgeInsets.only(left: 12.0),
              // child: ListTile(
              child: TextFormField(
                controller: _passwordTextController,
                obscureText: _hidePass,
                decoration: InputDecoration(
                    hintText: "Password",
                    icon: Icon(Icons.lock_outline),
                    border: InputBorder.none
                ),
                validator: (value) {
                  if (value.isEmpty) {
                    return "The password field cannot be empty";
                  } else if (value.length < 6) {
                    return "The password has to be at least 6 characters";
                  }
                  return null;
                },
              ),
              //),
            ),
          ),
        ),
      ];

    }else{
      return [
        Padding(
          padding: const EdgeInsets.fromLTRB(20.0, 20.0, 20.0, 0.0),
          child: Material(
            borderRadius: BorderRadius.circular(10.0),
            color: Colors.white.withOpacity(0.8),
            elevation: 0.0,
            child: Padding(
              padding: const EdgeInsets.only(left: 12.0),
              child: TextFormField(
                controller: _userNameController,
                decoration: InputDecoration(
                    hintText: "Username",
                    icon: Icon(Icons.add),
                    border: InputBorder.none
                  //border: OutlineInputBorder(),
                ),
              ),
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.fromLTRB(20.0, 20.0, 20.0, 0.0),
          child: Material(
            borderRadius: BorderRadius.circular(10.0),
            color: Colors.white.withOpacity(0.8),
            elevation: 0.0,
            child: Padding(
              padding: const EdgeInsets.only(left: 12.0),
              child: TextFormField(
                controller: _telefonController,
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                    hintText: "TelefonNummer",
                    icon: Icon(Icons.add_call),
                    border: InputBorder.none
                  //border: OutlineInputBorder(),
                ),validator: (value){
                if(value.isEmpty){
                  Pattern pattern =
                      r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
                  RegExp regex = new RegExp(pattern);
                  if(!regex.hasMatch(value))
                    return 'Please enter a valid telefon number';
                  else
                    return null;
                }
              },
              ),
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.fromLTRB(20.0, 20.0, 20.0, 0.0),
          child: Material(
            borderRadius: BorderRadius.circular(10.0),
            color: Colors.white.withOpacity(0.8),
            elevation: 0.0,
            child: Padding(
              padding: const EdgeInsets.only(left: 12.0),
              child: TextFormField(
                controller: _emailTextController,
                decoration: InputDecoration(
                    hintText: "Email",
                    icon: Icon(Icons.alternate_email),
                    border: InputBorder.none
                  //border: OutlineInputBorder(),
                ),
                validator: (value){
                  if(value.isEmpty){
                    Pattern pattern =
                        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
                    RegExp regex = new RegExp(pattern);
                    if(!regex.hasMatch(value))
                      return 'Please enter a valid email address';
                    else
                      return null;
                  }
                },
              ),
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.fromLTRB(20.0, 20.0, 20.0, 0.0),
          child: Material(
            borderRadius: BorderRadius.circular(10.0),
            color: Colors.white.withOpacity(0.8),
            elevation: 0.0,
            child: Padding(
              padding: const EdgeInsets.only(left: 12.0),
              child: ListTile(
                title: TextFormField(
                  controller: _passwordTextController,
                  obscureText: _hidePass,
                  decoration: InputDecoration(
                      hintText: "Password",
                      icon: Icon(Icons.lock_outline),
                      border: InputBorder.none),
                  validator: (value) {
                    if (value.isEmpty) {
                      return "The password field cannot be empty";
                    } else if (value.length < 6) {
                      return "The password has to be at least 6 characters";
                    }
                    return null;
                  },
                ),
                trailing: IconButton(
                    icon: Icon(
                      Icons.remove_red_eye,
                      size: 12.0,
                    ),
                    onPressed: () {

                      setState(() {
                        _hidePass = false;
                      });
                    }),
              ),
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.fromLTRB(20.0, 20.0, 20.0, 0.0),
          child: Material(
            borderRadius: BorderRadius.circular(10.0),
            color: Colors.white.withOpacity(0.8),
            elevation: 0.0,
            child: Padding(
              padding: const EdgeInsets.only(left: 12.0),
              child: ListTile(
                title: TextFormField(
                  controller: _confirmPasswordTextController,
                  obscureText: _hidePass,
                  decoration: InputDecoration(
                      hintText: "Confirm Password",
                      icon: Icon(Icons.lock_outline),
                      border: InputBorder.none),
                  validator: (value) {
                    if (value.isEmpty) {
                      return "The password field cannot be empty";
                    } else if (value.length < 6) {
                      return "The password has to be at least 6 characters";
                    } else if (value != _passwordTextController.text) {
                      return "the passwords do not match";
                    }
                  },
                ),
                trailing: IconButton(
                    icon: Icon(
                      Icons.remove_red_eye,
                      size: 12.0,
                    ),
                    onPressed: () {
                      setState(() {
                        _hidePass = false;
                      });
                    }),
              ),
            ),
          ),
        ),
      ];

    }

  }

  List<Widget> buildButtons(signinupbloc,context,email,password){
    if(_formMode == FormMode.LOGIN){
      return [
        Padding(
          padding: const EdgeInsets.fromLTRB(20.0, 20.0, 20.0, 0.0),
          child: Material(
            borderRadius: BorderRadius.circular(10.0),
            color: Colors.cyanAccent.withOpacity(0.8),
            elevation: 0.0,
            child: BlocBuilder<Loginbloc,UserState>(
            builder:(context,state) {
                      return   MaterialButton(
                        onPressed: () async{
                          if( validateAndSubmit()){

                            signinupbloc.add(Userlogin(email,password));

                          };
                        },
                        minWidth: MediaQuery.of(context).size.width,
                        child: Text(
                          "Login",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                              fontSize: 16.0),
                        ),
                      );
            })
          ),
        ),

        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Text(
            "Forgot password",
            textAlign: TextAlign.center,
            style: TextStyle(
                color: Colors.red,
                fontSize: 16.0,
                fontWeight: FontWeight.bold),
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: InkWell(
            onTap: _changeFormToSignUp,
            child: RichText(
                text: TextSpan(
                    style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.w400,
                        fontSize: 16.0),
                    children: [
                      TextSpan(
                          text:
                          "Don't have an account? click here to "),
                      TextSpan(
                          text: "Sign Up",
                          style: TextStyle(
                              color: Colors.cyan,
                              fontWeight: FontWeight.bold,
                              fontSize: 18.0))
                    ])),
          ),
        ),
      ];
    }else {
      return [
        Padding(
          padding: const EdgeInsets.fromLTRB(20.0, 20.0, 20.0, 0.0),
          child: Material(
            borderRadius: BorderRadius.circular(10.0),
            color: Colors.cyanAccent.withOpacity(0.8),
            elevation: 0.0,
            child: MaterialButton(
              onPressed: () async{
               // validateAndSubmit();
                if( validateAndSubmit()){
                  print("correct data");
                  print(user_.telefonNummer);
                  print(user_.email);
                  print(user_.userName);
                  print(user_.password);
                  print("correct data");
                  signinupbloc.add(Usersignup(user_));

                }else{
                  print("error data");
                };
              },
              minWidth: MediaQuery.of(context).size.width,
              child: Text(
                "Sign Up",
                textAlign: TextAlign.center,
                style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 16.0),
              ),
            ),
          ),
        ),

        Padding(
          padding: const EdgeInsets.all(8.0),
          child: InkWell(
            onTap:_changeFormToLogin,
            child: Text(
              "Login",
              textAlign: TextAlign.center,
              style: TextStyle(
                  color: Colors.lightBlue,
                  fontSize: 16.0,
                  fontWeight: FontWeight.bold),
            ),
          ),
        ),
      ];
    }

  }

}
