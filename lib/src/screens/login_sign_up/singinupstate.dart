
class UserState {
  @override
  List<Object> get props => [];

}

class UserIsNotConnected extends UserState{

}

class UserIsLoading extends UserState{

}

class UserIsConnected  extends UserState{
  final _user;
  UserIsConnected(this._user);
  String get  getUser =>_user;

  //List<Object> get props => [_user];
}
class UserIsNotLoaded  extends UserState{
  final error_message;
  UserIsNotLoaded(this.error_message);
  String get getMessage_error=>error_message;
}

class UserIsRegisted extends UserState{
  final response;
  UserIsRegisted(this.response);
}