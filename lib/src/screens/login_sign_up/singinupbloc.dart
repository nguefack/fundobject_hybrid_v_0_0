import 'package:bloc/bloc.dart';
import 'package:fund_app/src/Models/User.dart';
import 'package:fund_app/src/repository/login_repository.dart';
import 'package:fund_app/src/screens/login_sign_up/signinupevent.dart';
import 'package:fund_app/src/screens/login_sign_up/singinupstate.dart';

class Loginbloc extends Bloc<UserEvent,UserState>{
  Login login;
  Registration registration;
  Loginbloc(this.login,this.registration);

  @override
  UserState get initialState => UserIsNotConnected();

  @override
  Stream<UserState> mapEventToState(UserEvent event)  async*{

      if(event is Usersignup){
        yield UserIsLoading();
        try{
          var response= await registration.saveUserAuthenticationtable(event.user.userName,event.user.email,event.user.password,event.user.telefonNummer);
          yield UserIsRegisted(response);

        }catch(e){
          //var error_message=" enter a correct email address ";
          yield UserIsNotLoaded(e.toString());
        }

      }
      if(event is Userlogin){
        yield UserIsLoading();
      try{
        var userId = await  login.getUser( event.user_email,event.user_password );
        var response= await login.Start_Session_user(userId,event.user_email);
        yield UserIsConnected(response);

      }catch(e)
      {
        var error_message=" enter a correct email address ";
        if(e.toString()=="PlatformException(ERROR_USER_NOT_FOUND, There is no user record corresponding to this identifier. The user may have been deleted., null)"){

           error_message="email error login ";

        }else if(e.toString()=="PlatformException(ERROR_WRONG_PASSWORD, The password is invalid or the user does not have a password., null)"){
           error_message="password error";
        }else if(e.toString()== "PlatformException(ERROR_NETWORK_REQUEST_FAILED, A network error (such as timeout, interrupted connection or unreachable host) has occurred., null)"){
          error_message="_______ERROR _NETWORK checks your internet connection ";
        }

        yield UserIsNotLoaded(error_message);
      }

      }

    //return null;



  }

}