import 'package:equatable/equatable.dart';
import 'package:fund_app/src/Models/User.dart';

class UserEvent extends Equatable{
  @override
  List<Object> get props => [];
}
class Userlogin extends UserEvent{
  final user_email;
  final user_password;
  Userlogin(this.user_email,this.user_password);

//List<Object> get props => [user];
}
class Usersignup extends UserEvent{
 final User user;
  Usersignup(this.user);

}

class Resetuser extends UserEvent{

}
