import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:fund_app/src/Models/Objekt.dart';
import 'package:fund_app/src/Models/User.dart';
import 'package:fund_app/src/repository/login_repository.dart';
import 'package:fund_app/src/screens/chat/start_chat.dart';
import 'package:fund_app/src/screens/login_sign_up/login.dart';
import 'package:fund_app/src/screens/objekt_list/objekt_list_view_ausleihen.dart';
import 'package:fund_app/src/screens/widgets/image_ui.dart';
import 'package:url_launcher/url_launcher.dart';

class objectDetailView extends StatefulWidget {

  final Objekt objekt;

  objectDetailView({
    this.objekt,
});
  @override
  _objectDetailViewState createState() => _objectDetailViewState();
}

class _objectDetailViewState extends State<objectDetailView> {

  UserServices p = new UserServices();
  String message_error='',id_user_=null,name_user=null;
  Widget text_message=Text("") ;
  @override
  void initState() {

    p.getId_().then((onValue) {
      if (onValue != null) {
        setState(() {
          id_user_=onValue;
        });
      }
    });
    p.getUserName().then((onValue) {
      if (onValue != null) {
        setState(() {
          name_user=onValue;
        });
      }
    });
    super.initState();
  }
   @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        elevation: 0.0,
        title: Text('Details'),
        actions: <Widget>[
         ],
      ),

      body: ListView(
        children: <Widget>[
          Container(
            height: 300.0,
            child: GridTile(
              child: Container(
                color: Colors.white,
                child: new Image_url(image_url_:widget.objekt.image_name),
              ),
              footer: Container(
                color: Colors.white70,
                child: ListTile(

                  title: Center(
                    child: Text(
                      widget.objekt.name,
                      style: TextStyle(
                          fontWeight: FontWeight.w800, fontSize: 16.0),
                    ),
                  ),
                ),
              ),
            ),
          ),
    this.text_message,

         Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                IconButton(
                  icon: Icon(Icons.call, color: Colors.cyan),
                  onPressed: () => launch("tel://" + widget.objekt.tel_nummer.toString()),
                  // },
                ),
                Container(
                  margin: const EdgeInsets.only(top:8),
                  child: Text(
                    "CALL",
                    style: TextStyle(
                      fontSize: 12,
                      fontWeight: FontWeight.w400,
                      color:Colors.cyan,
                    ),
                  ),
                ),
              ],
            ),
            Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                IconButton(
                  icon: Icon(Icons.message, color: Colors.cyan),
                  onPressed: ()=> write_message()
                ),
                Container(
                  margin: const EdgeInsets.only(top:8),
                  child: Text(
                    "MESSAGE",
                    style: TextStyle(
                      fontSize: 12,
                      fontWeight: FontWeight.w400,
                      color:Colors.cyan,
                    ),
                  ),
                ),
              ],
            ),
            Column(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                IconButton(
                  icon: Icon(Icons.favorite_border, color: Colors.cyan),
                  onPressed: () {
                    add_to_favorite();
                    },
                ),
                Container(
                  margin: const EdgeInsets.only(top:8),
                  child: Text(
                    "Favorit",
                    style: TextStyle(
                      fontSize: 12,
                      fontWeight: FontWeight.w400,
                      color:Colors.cyan,
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),

          ListTile(
            title: Text("Adresse"),
            subtitle: Text( widget.objekt.strasse+","+widget.objekt.plz+" "+widget.objekt.stadt),

          ),
          ListTile(
            title: Text("Tel:"+widget.objekt.tel_nummer.toString()),
          ),
          ListTile(
            title: Text("Description"),
            subtitle: Text( widget.objekt.description),

          ),

        ],
      ),
    );
  }

  void write_message (){
          if(id_user_!=null){
            p.currentUser_info(id_user_).then( (value){
              value.once().then((DataSnapshot snap){
                var keys= snap.value.keys;
                var data= snap.value;
                for(var key in keys){

                  if(data[key]["userId"]== id_user_.toString() && id_user_!=widget.objekt.id_user.toString()){

                    String info_user='{"profil":"${data[key]['profil']}","my_objekts":"${data[key]['my_objekts']}","token_user":"${data[key]['token_user']}","userId":"${data[key]['userId']}","my_commandes":"${data[key]['my_commandes']}","telefon_nummer":"${data[key]['telefon_nummer']}","username":"${data[key]['username']}"}';
                    //String val='{"id":"${data[key]['username']}","id_2":"${data[key]['telefon_nummer']}"}';
                    //print(id_user_+"-------------<"+key+"--------------------------------------------->"+data[key]["userId"]);
                    Future<String> resultat= p.insert_id_object(info_user,key,data[key]["userId"]);

                    Navigator.of(context).push(new MaterialPageRoute(
                       builder: (BuildContext context) => new ChatWidget(id_receiver:widget.objekt.id_user,name_receiver:widget.objekt.name_user)));

                  }else{

                        if(id_user_==widget.objekt.id_user){
                          setState(() {
                            message_error="this is your item ";
                            this.text_message=new Center(
                                child:Text(message_error,style: TextStyle(color:Colors.red),)
                            );
                          });
                        }
                  }
                }
              });

              });
            }else{
             setState(() {
               message_error="you must first login ";
               this.text_message=new Center(
                   child:Text(message_error,style: TextStyle(color:Colors.red),)
               );
             });
          }

  }
  void add_to_favorite(){

    if(id_user_!=null){
      if(id_user_!=widget.objekt.id_user){
        Navigator.of(context).push(new MaterialPageRoute(
            builder: (BuildContext context) => new objektListViewAusleihen(redirect_to:1,object_:widget.objekt)));
      }else{
        setState(() {
          message_error="this is your item ";
          this.text_message=new Center(
              child:Text(message_error,style: TextStyle(color:Colors.red),)
          );
        });
      }
     }else{
      Navigator.of(context).push(new MaterialPageRoute(
          builder: (BuildContext context) => new   loginPage()));
    }
  }

}



