import 'package:bloc/bloc.dart';
import 'package:fund_app/src/repository/objekt_repository.dart';
import 'package:fund_app/src/screens/objekt_list/objektevents.dart';
import 'package:fund_app/src/screens/objekt_list/objektstate.dart';


class ObjektsBloc extends Bloc<ObjektsEvent,ObjektState>{
  @override
    Rufe_Objekten rufe_objekten;
  ObjektsBloc(this.rufe_objekten);
  get initialState => ObjektsAreNotSearched();

  @override
  Stream<ObjektState> mapEventToState(ObjektsEvent event) async*{

    if(event is SearchObjekts){

      yield ObjektsAreLoaded(event.name_objekt);
    }
    if(event is ListingAllObjekt){
      yield ObjektsAreLoading();
      var list= await rufe_objekten.get_ObjectList();
      yield ObjektsAreLoaded(list);
    }
  }

}