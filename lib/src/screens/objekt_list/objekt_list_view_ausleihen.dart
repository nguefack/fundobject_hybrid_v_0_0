import 'dart:io';

import 'package:flutter/material.dart';
import 'package:fund_app/src/Models/Objekt.dart';
import 'package:fund_app/src/repository/objekt_repository.dart';
import 'package:fund_app/src/Models/User.dart';
import 'package:fund_app/src/repository/login_repository.dart';
import 'package:fund_app/src/screens/Home_user/homeUser.dart';
import 'package:fund_app/src/screens/menu.dart';
import 'package:fund_app/src/screens/widgets/Loading.dart';

class objektListViewAusleihen extends StatefulWidget {
  int redirect_to;
  Objekt object_;
  objektListViewAusleihen({Key key,this.object_,this.redirect_to}):super(key :key);
  @override
  _objektListViewAusleihenState createState() => _objektListViewAusleihenState();
}

class _objektListViewAusleihenState extends State<objektListViewAusleihen> {
  Objekt o= new Objekt();
  Rufe_Objekten rufe_api= new Rufe_Objekten();
  Widget page_;
  UserServices p =new UserServices();

  @override
  void initState(){
    this.o=widget.object_;
    this.page_=lording_;
    save_objekt();
    super.initState();

  }
  void save_objekt(){

      p.getId_().then((onValue){
        if(onValue!=null){
          rufe_api.save_objekt_rental(onValue,this.o);
          setState(() {
            this.page_=new HomeUser();
          });
        }
      });

  }
  Widget get lording_ {
    return Scaffold(
      appBar: AppBar(
        title: Text('FundObject'),
        //  elevation: defaultTargetPlatform ==TargetPlatform.android ? 5.0:0.0,
        actions: <Widget>[
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Icon(Icons.notifications),)
        ],
      ),
      drawer: new Drawer(
        child: menu(),
      ),

      body: new Loading(),

    );
  }

  @override
  Widget build(BuildContext context) {
    if(widget.redirect_to==1){
      return  page_;
    }else{
      return Container(
        child: Center(child: Text("Same principle"),),
      );
    }

  }
}
