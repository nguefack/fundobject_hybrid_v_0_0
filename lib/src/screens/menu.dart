import 'package:flutter/material.dart';
import 'package:fund_app/main.dart';
import 'package:fund_app/src/repository/login_repository.dart';
import 'package:fund_app/src/screens/Home_user/homeUser.dart';
import 'package:fund_app/src/screens/create_object/Pages_object_form/Page_1_objekt_form.dart';
import 'package:fund_app/src/Models/User.dart';
import 'package:fund_app/src/screens/widgets/themes_colors.dart';
import 'package:fund_app/src/screens/login_sign_up/login.dart';
import 'package:shared_preferences/shared_preferences.dart';

class menu extends StatefulWidget {
  @override
  menu_body createState() => menu_body();
}

class menu_body extends State<menu> {
  UserServices userServices = new UserServices();
  String redirect = 'login_site';
  String action_login = "LogIn";
  String user_name="Name Vorname";
  String user_email="fundapp@";
  Color_app color_app = new Color_app();
  Color action_login_color = null;
  IconData action_login_icon;

  //Icon(Icons.exit_to_app, color: Colors.red,),
  @override
  void initState() {
    action_login_color = color_app.color_app_val;
    action_login_icon = Icons.assignment_ind;

    userServices.getId_().then((onValue) {
      if (onValue != null) {
        setState(() {
          redirect = onValue;
          action_login = 'Logout';
          action_login_color = color_app.color_app_error;
          action_login_icon = Icons.exit_to_app;
        });
      }
    });

    userServices.getUserName().then((onValue){
          if(onValue!=null){
            setState(() {
              user_name=onValue;
            });

          }
    });
    userServices.getUserEmail().then((onValue){
      if(onValue!=null){
        setState(() {
          user_email=onValue;
        });

      }
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new ListView(
      children: <Widget>[
        new UserAccountsDrawerHeader(
          accountName: new Text(user_name),
          accountEmail: new Text(user_email),
          currentAccountPicture: new CircleAvatar(
            backgroundColor: color_app.color_app_val,
            child: new Text(
              "S",
              style: TextStyle(color: Colors.white54, fontSize: 50),
            ),
          ),
        ),
        InkWell(
          onTap: () {
            Navigator.of(context).pop();
            Navigator.of(context).push(new MaterialPageRoute(
                builder: (BuildContext context) => new Myapp()));
          },
          child: ListTile(
            title: Text('Home Page'),
            leading: Icon(
              Icons.home,
              color: color_app.color_app_val,
            ),
          ),
        ),
        InkWell(
          child: new ListTile(
              title: new Text("Inserts Management"),
              leading: new Icon(
                Icons.person,
                color: color_app.color_app_val,
              ),
              onTap: () {
                Navigator.of(context).pop();
                if (redirect != 'login_site') {
                  Navigator.of(context).push(new MaterialPageRoute(
                      builder: (BuildContext context) => new HomeUser()));
                } else {
                  Navigator.of(context).push(new MaterialPageRoute(
                      builder: (BuildContext context) => new loginPage()));
                }
              }),
        ),
        InkWell(
          child: new ListTile(
            title: new Text("New Item"),
            leading: new Icon(
              Icons.plus_one,
              color: color_app.color_app_val,
            ),

            onTap: () {
              Navigator.of(context).pop();
              if (redirect != 'login_site') {
                Navigator.of(context).push(new MaterialPageRoute(
                    builder: (BuildContext context) => new Page_1_objekt()));
              } else {
                Navigator.of(context).push(new MaterialPageRoute(
                    builder: (BuildContext context) => new loginPage()));
              }
            },
          ),
        ),

        InkWell(onTap: () {
          if (redirect != 'login_site') {
            Navigator.of(context).push(new MaterialPageRoute(
                builder: (BuildContext context) => new HomeUser()));
          } else {
            Navigator.of(context).push(new MaterialPageRoute(
                builder: (BuildContext context) => new loginPage()));
          }
        },
          child: new ListTile(
            title: new Text("Favorites"),
            leading: new Icon(
              Icons.favorite,
              color: color_app.color_app_val,
            ),
          ),
        ),
        Divider(),
        InkWell(
          onTap: () {},
          child: ListTile(
            title: Text('Settings'),
            leading: Icon(
              Icons.settings,
              color: Colors.grey,
            ),
          ),
        ),
        InkWell(
          onTap: () {},
          child: ListTile(
            title: Text('About'),
            leading: Icon(
              Icons.help,
              color: Colors.grey,
            ),
          ),
        ),
        InkWell(
          onTap: () {
            disconnect_();
          },
          child: ListTile(
            title: Text(action_login),
            leading: Icon(
              action_login_icon,
              color: action_login_color,
            ),
          ),
        ),
      ],
    );
  }

  void disconnect_() {
    if (action_login == 'Logout') {
      userServices.logoutUser();
      Navigator.pop(context);
      Navigator.push(context, MaterialPageRoute(builder: (context) => Myapp()));
    } else {
      Navigator.pop(context);
      Navigator.push(
          context, MaterialPageRoute(builder: (context) => loginPage()));
    }
  }
}
