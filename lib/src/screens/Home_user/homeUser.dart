import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:fund_app/src/Models/Objekt.dart';
import 'package:fund_app/src/repository/objekt_repository.dart';
import 'package:fund_app/src/Models/User.dart';
import 'package:fund_app/src/repository/login_repository.dart';
import 'package:fund_app/src/screens/Home_user/liste_publication.dart';
import 'package:fund_app/src/screens/Home_user/favorite.dart';
import 'package:fund_app/src/screens/chat/customer.dart';
import 'package:fund_app/src/screens/menu.dart';
import 'package:fund_app/src/screens/widgets/themes_colors.dart';

class HomeUser extends StatefulWidget{

  @override
  HomeUser_main createState() => HomeUser_main();

}

class HomeUser_main extends State<HomeUser> {
  Color_app color_app= new Color_app();
  List<Objekt> ObjektList=[];
  List<Objekt> Liste_favorites=[];
  UserServices p =new UserServices();
  String id_user_=null;
  Rufe_Objekten rufe_objekten=new Rufe_Objekten();
  @override
  void initState() {
    p.getId_().then((onValue){
      if(onValue!=null){
        setState(() {
          id_user_=onValue;
          getListeObjekts();
        });
      }
    });
    super.initState();
  }
  void getListeObjekts()async {
    var list= await rufe_objekten.get_ObjectList_publication(id_user_);
    var list_f= await rufe_objekten.get_ObjectList_favorite(id_user_);
    setState(() {
      this.ObjektList= list;
      this.Liste_favorites= list_f;
    });

  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: new ThemeData(primarySwatch: Colors.lime,
          primaryColor: defaultTargetPlatform ==
              TargetPlatform.iOS ? color_app.color_app_def_val
              : color_app.color_app_def_val),
      debugShowCheckedModeBanner: false,

      home: DefaultTabController(
        length: choices.length,
        child: Scaffold(
          appBar: AppBar(
            title: const Text('Inserts Management'),
            actions: <Widget>[
              Padding(
                padding: const EdgeInsets.all(16.0),
                child: Icon(Icons.search),)
            ],
            bottom: TabBar(
              isScrollable: true,
              tabs: choices.map((Choice choice) {
                return Tab(
                  text: choice.title,
                  icon: Icon(choice.icon),
                );
              }).toList(),
            ),
          ),
          drawer: new Drawer(
            child: menu(),
          ),

          body: TabBarView(

            children: choices.map((Choice choice) {
              return Padding(

                padding: const EdgeInsets.all(16.0),
                child: ChoiceCard(choice: choice,Liste_publikations:this.ObjektList,Liste_favorite: this.Liste_favorites,),

              );
            }).toList(),
          ),
        ),
      ),
    );
  }
}

class Choice {
  const Choice({this.title, this.icon});

  final String title;
  final IconData icon;
}

const List<Choice> choices = const <Choice>[
  const Choice(title: 'Publication', icon: Icons.library_books),
  const Choice(title: 'Favorites ', icon: Icons.collections_bookmark),
  const Choice(title: 'Messages', icon: Icons.supervised_user_circle),
];

class ChoiceCard extends StatelessWidget {
  const ChoiceCard({Key key, this.choice,this.Liste_publikations,this.Liste_favorite}) : super(key: key);

  final Choice choice;
  final Liste_publikations;
  final Liste_favorite;

  @override
  Widget build(BuildContext context) {
    final TextStyle textStyle = Theme.of(context).textTheme.display1;
    if(choice.icon==Icons.library_books){
      return new Liste_publikation(my_objects:this.Liste_publikations);
    }
    if(choice.icon==Icons.supervised_user_circle){
      return new Customers(my_objects:this.Liste_publikations);
    }
    if(choice.icon==Icons.collections_bookmark){
      return new Liste_favorite_(my_objects:this.Liste_favorite);
    }
    return Card(
      color: Colors.white,
      child: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Icon(choice.icon, size: 128.0, color: textStyle.color),
            Text(choice.title, style: textStyle),
          ],
        ),
      ),
    );


  }
}
