import 'package:flutter/material.dart';
import 'package:fund_app/src/Models/Objekt.dart';
import 'package:fund_app/src/screens/widgets/SingleObject.dart';

class Objekt_list_ui extends StatelessWidget{
  List<Objekt> ObjektList;
  Objekt_list_ui({this.ObjektList});

  @override
  Widget build(BuildContext context) {

    return objektListView_;
  }

  Widget get objektListView_{

    return GridView.builder(
      itemCount: ObjektList.length,
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 2),
      itemBuilder: (BuildContext context, int index){
        return single_object(
          object_:ObjektList[index],
        );
      },
    );
  }
}