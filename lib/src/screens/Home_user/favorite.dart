import 'package:flutter/material.dart';
import 'package:fund_app/src/Models/Objekt.dart';
import 'package:fund_app/src/screens/Home_user/objekt_list_ui.dart';

class Liste_favorite_ extends StatefulWidget{
  @override
  List<Objekt> my_objects;
  Liste_favorite_({Key  key,this.my_objects}):super(key:key);

  body_liste createState()=> body_liste();

}
class body_liste extends State<Liste_favorite_>{


  @override
  Widget build(BuildContext context) {

    return Objekt_list_ui(ObjektList:widget.my_objects);
  }
}