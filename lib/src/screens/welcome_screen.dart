import 'dart:async';
import 'package:fund_app/src/repository/objekt_repository.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fund_app/src/repository/login_repository.dart';
import 'package:fund_app/src/screens/menu.dart';
import 'package:fund_app/src/Models/Objekt.dart';
import 'package:fund_app/src/screens/objekt_list/objektstate.dart';
import 'package:fund_app/src/screens/widgets/single_object_horizontal_ui.dart';
import 'package:fund_app/src/screens/widgets/category_filter_ui.dart';
import 'package:fund_app/src/screens/widgets/single_object_vertical.dart';
import 'package:fund_app/src/screens/widgets/themes_colors.dart';

import 'objekt_list/objektbloc.dart';
import 'objekt_list/objektevents.dart';

class WelcomeScreen extends StatefulWidget {
  List<Objekt> item_list;
  WelcomeScreen({
    this.item_list,
  });

  @override
  _WelcomeScreenState createState() => _WelcomeScreenState();
}

class _WelcomeScreenState extends State<WelcomeScreen> {
  Color_app color_app= new Color_app();
  List<Objekt> item_list=[];
  String id_user_;
  UserServices p = new UserServices();

  @override
  void initState() {

    p.getId_().then((onValue) {
      if (onValue != null) {
        setState(() {
          id_user_=onValue;
        });
      }
    });
 super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      builder: (context)=>ObjektsBloc(Rufe_Objekten()),
      child: BlocBuilder<ObjektsBloc,ObjektState>(
        builder: (context,state){
            return MaterialApp(
              theme: new ThemeData(primarySwatch: Colors.lime,
                  primaryColor: defaultTargetPlatform ==
                      TargetPlatform.iOS ? color_app.color_app_def_val
                      : color_app.color_app_def_val),
              debugShowCheckedModeBanner: false,
              home: new Scaffold(
                  appBar: new AppBar(
                    elevation: 0.0,
                     title: Text('FundApp'),
                    actions: <Widget>[
                      new IconButton(
                          icon: Icon(
                            Icons.search,
                            color: Colors.white,
                          ),
                          onPressed: () {
                             showSearch(context: context, delegate: SearchObjekt());
                          }),
                    ],
                  ),
                  drawer: new Drawer(
                    child: menu(),
                  ),

                  body: BlocBuilder<ObjektsBloc,ObjektState>(
                      builder:(context,state) {
                         if(state is ObjektsAreNotSearched){
                           final signinupbloc=BlocProvider.of<ObjektsBloc>(context);
                            signinupbloc.add(ListingAllObjekt());
                           return Center(child: CircularProgressIndicator());
                         }else if(state is ObjektsAreLoading) {
                           return Center(child: CircularProgressIndicator());
                         }else if(state is ObjektsAreLoaded){

                                item_list=state.list_;

                           return new Material(

                               child: new SingleChildScrollView(
                                   child: new ConstrainedBox(
                                     constraints: new BoxConstraints(),
                                     child: new Column(children: <Widget>[

                                       //Horizontal View
                                       Container(height: 900.0,
                                           child: ListView.builder(
                                               itemCount: item_list.length,
                                               itemBuilder: (context, index){
                                                 if(index==0){
                                                   return Column(

                                                     children: <Widget>[

                                                       CategoryFilterUI(),
                                                       Padding(
                                                         padding: const EdgeInsets.fromLTRB(2.0, 5.0, 0.0, 2.0),
                                                         child: Container(
                                                             alignment: Alignment.topLeft,
                                                             child: Text(
                                                               // "${_adress?.addressLine?? '-'}"+"tito",
                                                               "Top Recent Items",
                                                               style: TextStyle(),
                                                             )),
                                                       ),
                                                       SingleObjectHorizontalUi(),

                                                       singleObjectViewVertical(object_: item_list[index]),
                                                     ],
                                                   );
                                                 }else{
                                                  /* if(index==item_list.length-2){
                                                     return Container(
                                                       child:Column(
                                                         children: <Widget>[
                                                              singleObjectViewVertical(object_: item_list[index]),
                                                              singleObjectViewVertical(object_: item_list.last),
                                                         ],
                                                       ) ,
                                                     );
                                                   }
                                                   
                                                   */
                                                   return  singleObjectViewVertical(object_: item_list[index]);
                                                 }


                                               })

                                       ),
                                       Container(
                                         child:singleObjectViewVertical(object_: item_list.last) ,
                                       ),

                                     ]),
                                   )));
                          }else if(state is OjektsError){
                           return Center(child : Text("error"));
                         }
                         return Center(child : Text("Welcom"));


                 } )
              ),
            );;

        },
      ),
    );
  }

}
class SearchObjekt extends SearchDelegate<String>{
  @override
  List<Widget> buildActions(BuildContext context) {
    // TODO: implement buildActions
    return null;
  }

  @override
  Widget buildLeading(BuildContext context) {
    // TODO: implement buildLeading
    return null;
  }

  @override
  Widget buildResults(BuildContext context) {
    // TODO: implement buildResults
    return null;
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    // TODO: implement buildSuggestions
    return null;
  }

}