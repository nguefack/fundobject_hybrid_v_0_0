import 'dart:io';
import 'package:fund_app/src/repository/objekt_repository.dart';
import 'package:flutter/material.dart';
import 'package:fund_app/src/Models/Objekt.dart';
import 'package:fund_app/src/Models/User.dart';
import 'package:fund_app/src/repository/login_repository.dart';
import 'package:fund_app/src/screens/Home_user/homeUser.dart';
import 'package:fund_app/src/screens/menu.dart';
import 'package:fund_app/src/screens/widgets/Loading.dart';

class Objekt_erstellen extends StatefulWidget{
  final state=Object_controler();
  File file;
  Objekt objekt_;
  Objekt_erstellen({this.file,this.objekt_});

  @override
  Object_controler createState()=>state;
}

class Object_controler extends State<Objekt_erstellen>{
  Objekt o= new Objekt();
  Rufe_Objekten rufe_api= new Rufe_Objekten();
  Widget page_;
  String image_name,title='';
  File file_;
  UserServices p =new UserServices();

  @override
  void initState(){
    this.o=widget.objekt_;
    this.page_=lording_;
    this.file_=widget.file;
    rufe_api.save_file_firebase(this.file_).then(save_objekt);
    super.initState();

  }

  @override
  void deactivate() {
    // TODO: implement deactivate
    super.deactivate();
  }
  void save_objekt(String val){

    if(val!="error"){
      p.getId_().then((onValue){
        if(onValue!=null){
          this.o.image_name=val;
          this.o.id_user=onValue;
          p.getUserName().then((onValue){
            if(onValue!=null){
              setState(() {
                this.o.name_user=onValue;
                rufe_api.save_objekt_(this.o);
                this.page_=new HomeUser();
                print(onValue);
                print("carlos_ save");

              });

            }
          });
        }
      });

    }else {
      print(val + "error ____");

    }
  }

  Widget get lording_ {
    return Scaffold(
      appBar: AppBar(
          title: Text('FundObject'),
          //  elevation: defaultTargetPlatform ==TargetPlatform.android ? 5.0:0.0,
          actions: <Widget>[
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: Icon(Icons.notifications),)
          ],
      ),
      drawer: new Drawer(
        child: menu(),
      ),

      body: new Loading(),

    );
  }

  @override
  Widget build(BuildContext context) {

    return page_;

  }
}

