import 'dart:async';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:fund_app/src/Models/Objekt.dart';
import 'package:fund_app/src/screens/menu.dart';
import 'package:fund_app/src/screens/create_object/Pages_object_form/Page_2_objekt_form.dart';
import 'package:fund_app/src/screens/create_object/create_o.dart';
import 'package:fund_app/src/screens/widgets/themes_colors.dart';
import 'package:image_picker/image_picker.dart';
import 'package:geocoder/geocoder.dart';
import 'package:geolocator/geolocator.dart';


class Page_3_objekt extends StatefulWidget{
File file;
Objekt objekt_;
  Page_3_objekt(this.file,this.objekt_);
  @override
  _ImageState createState()=> _ImageState();
}

class _ImageState extends State<Page_3_objekt>{
  Color_app color_app= new Color_app();
  File fl;
  Objekt s= new Objekt();
  String plz,strasse,stadt;
  int tel_nummer;
  Position _position;
  StreamSubscription<Position> _streamSubscription;
  Address _adress;
  bool _value=false;
  var _formkey= GlobalKey<FormState>();
  var object_name=  new TextEditingController();
  var object_plz=  new TextEditingController();
  var object_strasse =  new TextEditingController();
  var object_stadt=  new TextEditingController();
  File _imageFile;
  String title_="Adresse:How to find the object?";
  String title2_="Use your current position?";
  String title_erro="";

@override
void initState(){
   this.fl=widget.file;
   this.s=widget.objekt_;
   // call_location();
    super.initState();

}

  @override

  String call_location(){
    var locationOption= LocationOptions(accuracy: LocationAccuracy.high,distanceFilter: 10);
    _streamSubscription=Geolocator().getPositionStream(locationOption).listen((Position position){
      setState(() {
        _position=position;

        final coordinates= new Coordinates(position.latitude,position.longitude);
        convertCoordinatesToAdress(coordinates).then((value) {
          setState(() {
            _adress=value;
            object_plz.text=_adress.postalCode;
            object_strasse.text=_adress.thoroughfare+""+_adress.featureName;
            object_stadt.text=_adress.locality;
          });
        });
      });
    });
    return _adress.toString();
  }
  Future<Address> convertCoordinatesToAdress(Coordinates coordinates) async{
    var addresses = await Geocoder.local.findAddressesFromCoordinates(coordinates);
    return addresses.first;
  }
  void redirectToPage3(BuildContext context){
    setState(() {
      var rout = new MaterialPageRoute(builder:(BuildContext context)=> new Page_2_objekt(s));
      Navigator.of(context).push(rout);
    });
  }
  void onChanged(bool value){
    if(value=true){
      var _address_=call_location();
      if(_address_=="null"){
        setState(() {
          title_erro="activate or check your GPS sensor";
        });
      }else{
        setState(() {
          title_erro="";
        });
      }
    }else{
      setState(() {

        object_plz.text="1";
        object_strasse.text="";
        object_stadt.text="";
      });
    }
    setState(() {
      _value=value;
    });
  }
  void _submitForm() {
    _formkey.currentState.save();

    this.s.tel_nummer=this.tel_nummer;
    this.s.plz=this.plz;
    this.s.strasse=this.strasse;
    this.s.stadt=this.stadt;
    this.s.date=formatDate(DateTime.now());

    Navigator.of(context).pop();
    setState(() {
      var rout = new MaterialPageRoute(builder:(BuildContext context)=> new Objekt_erstellen(file:this.fl,objekt_:this.s));
      Navigator.of(context).push(rout);

    });
  }

  @override
  Widget build(BuildContext context) {

    final screenZise=MediaQuery.of(context).size;

    return new Scaffold(
      appBar:  AppBar(
          title: Text('FundObject'),
          actions:<Widget>[
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: Icon(Icons.notifications),
            )
          ]
      ),
      drawer : new Drawer(
        child:   menu(),
      ),
      body: new Container(
        child: ListView(
          scrollDirection: Axis.horizontal,
          children: <Widget>[
            new Container(
                width: screenZise.width,
                height: screenZise.height,
                color:color_app.color_app_white,
                child:Padding(
                    padding: const EdgeInsets.all(2.0),
                    child:Form(
                      key:_formkey ,
                      child:SingleChildScrollView(
                        padding: const EdgeInsets.symmetric(horizontal: 15.0),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          crossAxisAlignment: CrossAxisAlignment.stretch,

                          children: <Widget>[
                            SizedBox(height: 24.0),
                            //SizedBox(height: 24.0),
                            Padding(
                              padding: const EdgeInsets.fromLTRB(0.0, 0.0, 8.0, 4.0),
                              child: Container(
                                alignment: Alignment.topRight,
                                child: new CircleAvatar(
                                  backgroundColor: color_app.color_app_def_val,
                                  child: new Text("3/3",
                                    style:TextStyle(color: Colors.white54,fontSize: 20),
                                  ),
                                ),
                              ),
                            ),
                            Text(title_),

                            SizedBox(height: 14.0),
                            TextFormField(
                              keyboardType: TextInputType.phone,
                              controller: object_name,
                              validator: (String value){
                                if(value.isEmpty){
                                  return 'pleace enter your telephone number';
                                }
                              },
                              onSaved: (String val) => this.tel_nummer = int.parse(val),
                              decoration: const InputDecoration(
                                border: OutlineInputBorder(),
                                labelText: "Telephone number*",



                              ),

                            ),
                            SizedBox(height: 14.0),
                            Row(
                              children: <Widget>[
                                Text(title2_),
                                new Switch(value: _value, onChanged:(bool value){onChanged(value);}),
                              ],
                            ),
                            Text(title_erro,style: TextStyle(
                                color: color_app.color_app_error,
                                fontSize: 20.0
                            ),),

                            Material(
                              child: Padding(
                                padding: const EdgeInsets.fromLTRB(8.0, 0.0, 8.0, 0.0),
                                child: TextFormField(

                                  textCapitalization: TextCapitalization.words,
                                  controller: object_plz,
                                  validator: (String value){
                                    if(value.isEmpty){
                                      return 'pleace enter the Postal Code';
                                    }
                                  },
                                  onSaved: (String val) => this.plz = val,
                                  decoration: InputDecoration(
                                    border: UnderlineInputBorder(),

                                    labelText: "Postal Code*",
                                  ),

                                ),
                              ),
                            ),
                            SizedBox(height: 14.0),
                            Material(
                              color: Colors.white.withOpacity(0.8),
                              elevation: 0.0,
                              child: Padding(
                                padding: const EdgeInsets.fromLTRB(8.0, 0.0, 8.0, 0.0),
                                child: TextFormField(

                                  textCapitalization: TextCapitalization.words,
                                  controller: object_strasse,
                                  validator: (String value){
                                    if(value.isEmpty){
                                      return 'pleace enter your street';
                                    }
                                  },
                                  onSaved: (String val) => this.strasse = val,
                                  decoration: InputDecoration(
                                    border: UnderlineInputBorder(),
                                    labelText: "Street*",
                                  ),
                                   //validator: _valid_Name
                                ),
                              ),
                            ),
                            SizedBox(height: 14.0),
                            Material(
                              color: Colors.white.withOpacity(0.8),
                              elevation: 0.0,
                              child: Padding(
                                padding: const EdgeInsets.fromLTRB(8.0, 0.0, 8.0, 0.0),
                                child: TextFormField(

                                  textCapitalization: TextCapitalization.words,
                                  controller: object_stadt,
                                  validator: (String value){
                                    if(value.isEmpty){
                                      return 'pleace enter your city';
                                    }
                                  },
                                  onSaved: (String val) => this.stadt = val,
                                  decoration: InputDecoration(
                                    labelText: "City*",
                                  ),
                                  //validator: _valid_Name
                                ),
                              ),
                            ),
                            SizedBox(height: 54.0),
                                             Padding(
                              padding: const EdgeInsets.all(0.0),
                              child: Material(
                                  borderRadius: BorderRadius.circular(10.0),
                                  color: color_app.color_app_def_val.withOpacity(0.8),
                                  elevation: 0.0,
                                  child:  MaterialButton(
                                    minWidth: MediaQuery.of(context).size.width,
                                    child: Text('save ',
                                        style:TextStyle(
                                            color: color_app.color_app_white,
                                            fontSize: 20.0
                                        )),
                                    onPressed:() {

                                      if(_formkey.currentState.validate()){
                                        _submitForm();
                                      }
                                    },
                                  )
                              ),
                            ),
                            SizedBox(height: 54.0),

                          ],
                        ),
                      ) ,
                    )
                )
            ),

          ],
        ),

      ),
    );

  }
  Future<Null> _pickImageFromGallery() async{
    final File imageFile= await ImagePicker.pickImage(source: ImageSource.gallery);
    setState(() {
      this._imageFile=imageFile;
    });

  }
  Future<Null> _pickImageFromCamera() async{
    final File imageFile= await ImagePicker.pickImage(source: ImageSource.camera  );
    setState(() {
      this._imageFile=imageFile;
    });

  }
  String formatDate(DateTime d) {
    var month = d.month;
    var day = d.day;
    var year = d.year;
    return "$month/$day/$year";
  }

}
