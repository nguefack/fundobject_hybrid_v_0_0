import 'package:flutter/material.dart';
import 'package:fund_app/src/Models/Objekt.dart';
import 'package:fund_app/src/screens/menu.dart';
import 'package:fund_app/src/screens/create_object/Pages_object_form/Page_2_objekt_form.dart';
import 'package:fund_app/src/screens/widgets/themes_colors.dart';

class Page_1_objekt extends StatefulWidget {
  Page_1_objekt();
  @override
  _MyStatefulWidgetState createState() => _MyStatefulWidgetState();

}
class _MyStatefulWidgetState extends State<Page_1_objekt> {
  Color_app color_app= new Color_app();
  var _formkey= GlobalKey<FormState>();
  int selected_value;
  String dropdownValue,error;
  var object_name=  new TextEditingController();
  var object_price= new TextEditingController();
  var object_description= new TextEditingController();
  final  List<String> list=[
    'Choose category',
    'ID Cards',
    'Vehicles',
    'Supplies and Materials',
    'Other'
  ];
  @override
  void initState(){
    super.initState();
    this.selected_value=0;
    this.error='';
    this.dropdownValue = 'Choose category';
  }
  setSelectedRadio( int value){
    setState(() {
      selected_value=value;
    });
  }

  void _submit(BuildContext context){
    if(this.dropdownValue!='Choose category' && this.selected_value!=0){
      Objekt o=new Objekt();
      setState(() {
        o.name=object_name.text;
        o.price=int.parse(object_price.text);
        o.description=object_description.text;
        o.action=this.selected_value;
        o.category=this.dropdownValue;
        var route=new  MaterialPageRoute(builder: (BuildContext context)=> new Page_2_objekt(o));
        Navigator.of(context).push(route);
      });


    }else{
      setState(() {
        if(this.selected_value==0){
          this.error ='pleace choise the action';
        }else{
          this.error ='pleace choose category ';
        }
      });
    }
  }

  Widget build(BuildContext context) {

   TextStyle textStyle= Theme.of(context).textTheme.title;
    final dropMenu=
    DropdownButtonHideUnderline(
        child: Padding(
          padding: const EdgeInsets.fromLTRB(8.0, 30.0, 0.0, 10.0),
          child: DropdownButton<String>(
            value: dropdownValue,
            iconSize: 40.0,
            onChanged: (String newValue) {
              setState(() {
                dropdownValue = newValue;
              });
            },
            items: list
                .map<DropdownMenuItem<String>>((String value) {
              return DropdownMenuItem<String>(
                value: value,
                child: Text(value, style: TextStyle(fontSize: 20.0),),
              );
            })
                .toList(),
          ),
        )
    );

   final _name = Material(
     color: Colors.white.withOpacity(0.8),
     elevation: 0.0,
     child: Padding(
         padding: const EdgeInsets.fromLTRB(8.0, 0.0, 8.0, 0.0),
         child: TextFormField(
           keyboardType: TextInputType.text,
           style: textStyle,
           controller: object_name,
           validator: (String value){
             if(value.isEmpty){
               return 'pleace enter the object name';
             }
           },
           decoration: const InputDecoration(
             //border: OutlineInputBorder(),
               labelText: "Name*",
               errorStyle: TextStyle(
                   color: Colors.red,
                   fontSize: 15.0
               )
           ),
         ),
     ),
   );

    final _action=  Padding(
        padding: const EdgeInsets.all(0.0),
        child:Row(
          children: <Widget>[
            Expanded(
                child: ListTile(
                    title: Text(
                      "Found",
                      textAlign: TextAlign.start,
                    ),
                    trailing: Radio(
                        value: 2,
                        groupValue: selected_value,
                        activeColor: color_app.color_app_def_val,
                        onChanged: (val){
                          setSelectedRadio(val);
                        }
                    )
                )
            ),
            Expanded(
                child: ListTile(
                    title: Text(
                      "Lend",
                      textAlign: TextAlign.start,
                    ),
                    trailing: Radio(
                        value: 1,
                        groupValue: selected_value,
                        activeColor: color_app.color_app_def_val,
                        onChanged: (val){
                          setSelectedRadio(val);
                        }
                    )
                )
            ),

          ],
        )
    );
    final _description= Material(
      borderRadius: BorderRadius.circular(10.0),
      color: Colors.white.withOpacity(0.8),
      elevation: 0.0,
      child: Padding(
          padding: const EdgeInsets.fromLTRB(8.0, 0.0, 8.0, 0.0),
          child: TextFormField(
            textCapitalization: TextCapitalization.words,
            controller: object_description,
            validator: (String value){
              if(value.isEmpty){
                return 'pleace briefly describe the state of the object';
              }
            },
            decoration: InputDecoration(
                border: OutlineInputBorder(),
                hintText: "Tell us about The object state ",
                labelText: "Description*",
                errorStyle: TextStyle(
                    color: color_app.color_app_error,
                    fontSize: 15.0
                )
            ),
            maxLines: 3,
          ),
      ),
    );
    final _next_button= Padding(
        padding: const EdgeInsets.fromLTRB(8.0, 0.0, 8.0, 0.0),
      child: Material(
          borderRadius: BorderRadius.circular(10.0),
          color: color_app.color_app_def_val.withOpacity(0.8),
          elevation: 0.0,
          child:  MaterialButton(
            minWidth: MediaQuery.of(context).size.width,
            child: Text('next ',
                style:TextStyle(
                    color: color_app.color_app_white,
                    fontSize: 20.0
                )),
            onPressed:() {

              if(_formkey.currentState.validate()){

                _submit(context);
              }
            },

          )
      ),
    );

    final val=Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        SizedBox(height: 0.0),
         new Center(
           child: Text(
             this.error,
             style: TextStyle(color: color_app.color_app_error),
           ),
         ),
        //SizedBox(height: 24.0),
        Padding(
          padding: const EdgeInsets.only(right:28.0),
          child: Container(
            alignment: Alignment.topRight,
            child: new CircleAvatar(
              backgroundColor: color_app.color_app_def_val,
              child: new Text("1/3",
                style:TextStyle(color: Colors.white54,fontSize: 20),
              ),
            ),
          ),
        ),
        _name,
        dropMenu,
        _action,

        //SizedBox(height: 10.0),
       _price,
      // SizedBox(height: 24.0),
       _description,
        SizedBox(height: 34.0),
        _next_button,
        SizedBox(height: 24.0),
      ],
    );

   final screenZise=MediaQuery.of(context).size;

    return new Scaffold(
      appBar:  AppBar(
          title: Text('FundObject'),
          actions:<Widget>[
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: Icon(Icons.notifications),)
          ]
      ),
      drawer : new Drawer(
        child:   menu(),
      ),
      body: new Container(
        child: ListView(
          scrollDirection: Axis.horizontal,
          children: <Widget>[
            new Container(
              width: screenZise.width,
              height: screenZise.height,
              color:color_app.color_app_white,
              child: Form(
                key: _formkey,
                child:SingleChildScrollView(
                  padding: const EdgeInsets.symmetric(horizontal: 15.0),
                  child: val ,
                ) ,
              ),
            ),
          ],
        ),
      ),
    );
    }


  Widget get _price{
    return  Material(
      borderRadius: BorderRadius.circular(10.0),
      color: Colors.white.withOpacity(0.8),
      elevation: 0.0,
      child: Padding(
        padding: const EdgeInsets.fromLTRB(8.0, 0.0, 8.0, 30.0),
        child: TextFormField(
          keyboardType: TextInputType.number,
          controller: object_price,
          validator: (String value){
            if(value.isEmpty){
              return 'pleace enter the price: 0 if the object is free of charge';
            }
          },
          decoration: const InputDecoration(
              //border: OutlineInputBorder(),
              labelText: "Price( € )*",
              suffixText: 'Euro (€)',
              suffixStyle:TextStyle(color:Colors.cyan),
              errorStyle: TextStyle(
                  color: Colors.red,
                  fontSize: 15.0
              )
          ),
          maxLines:1,
        ),
      ),
    );
  }



}

