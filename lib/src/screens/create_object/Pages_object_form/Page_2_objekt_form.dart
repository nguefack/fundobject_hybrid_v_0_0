import 'dart:io';

import 'package:flutter/material.dart';
import 'package:fund_app/src/Models/Objekt.dart';
import 'package:fund_app/src/screens/menu.dart';
import 'package:fund_app/src/screens/create_object/Pages_object_form/Page_3_objekt_form.dart';
import 'package:fund_app/src/screens/widgets/themes_colors.dart';
import 'package:image_picker/image_picker.dart';

class Page_2_objekt extends StatefulWidget{
  Objekt objekt_;
  Page_2_objekt(this.objekt_);
  @override
  _ImageState createState()=> _ImageState();
}

class _ImageState extends State<Page_2_objekt>{
    Color_app color_app= new Color_app();
 Text title;
 Objekt o;
 File _imageFile=null;

Widget get title_error{
  return Text(" pleace take picture first ",
    style: TextStyle(
      color: color_app.color_app_error,
      fontSize:15.0,
    ),
  );
}
@override
void initState(){
  this.o=widget.objekt_;
    super.initState();
  }

  void redirectToPage3(BuildContext context){
      if(_imageFile!=null){
        setState(() {
          var rout = new MaterialPageRoute(builder:(BuildContext context)=> new Page_3_objekt(_imageFile,this.o));
          Navigator.of(context).push(rout);
        });
      }else{
        setState(() {
          title=title_error;

        });
      }
  }

  @override
  Widget build(BuildContext context) {

    final screenZise=MediaQuery.of(context).size;

    return new Scaffold(
      appBar:  AppBar(
          title: Text('FundObject'),
          actions:<Widget>[
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: Icon(Icons.notifications),)
          ]
      ),
      drawer : new Drawer(
        child:   menu(),
      ),
      body: new Container(
        child: ListView(
          scrollDirection: Axis.horizontal,
          children: <Widget>[
            new Container(
                width: screenZise.width,
                height: screenZise.height,
                color:color_app.color_app_white,
                child:Padding(
                    padding: const EdgeInsets.fromLTRB(0.0, 1.0, 8.0, 0.0),
                    child:ListView(
                      padding: const EdgeInsets.symmetric(horizontal: 10.0),
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.fromLTRB(0.0, 10.0, 8.0, 8.0),
                          child: Container(
                            alignment: Alignment.topRight,
                            child: new CircleAvatar(
                              backgroundColor: color_app.color_app_def_val,
                              child: new Text("2/3",
                                style:TextStyle(color: Colors.white54,fontSize: 20),
                              ),
                            ),
                          ),
                        ),
                        this._imageFile==null ? Placeholder(strokeWidth: 0.8, color: Colors.grey,) : Image.file(this._imageFile),

                        SizedBox(height: 14.0),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            Column(
                              mainAxisSize: MainAxisSize.min,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                IconButton(
                                  icon: Icon(Icons.add_a_photo, color: Colors.cyan[600]),
                                  onPressed: () async=>await _pickImageFromCamera(),
                                ),
                                Container(
                                  //margin: const EdgeInsets.only(top:8),
                                  child: Text(
                                    "TAKE A PICTURE",
                                    style: TextStyle(
                                      fontSize: 12,
                                      fontWeight: FontWeight.w400,
                                      color:Colors.cyan[600],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            Column(
                              mainAxisSize: MainAxisSize.min,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                IconButton(
                                  icon: Icon(Icons.photo, color: Colors.cyan[600]),
                                  onPressed: () async=>await _pickImageFromGallery(),

                                ),
                                Container(
                                 // margin: const EdgeInsets.only(top:8),
                                  child: Text(
                                    "CHOOSE FROM GALLERY",
                                    style: TextStyle(
                                      fontSize: 12,
                                      fontWeight: FontWeight.w400,
                                      color:Colors.cyan[600],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),

                    Padding(
                      padding: const EdgeInsets.fromLTRB(8.0, 40.0, 8.0, 0.0),
                      child: Material(
                          borderRadius: BorderRadius.circular(10.0),
                          color: color_app.color_app_def_val.withOpacity(0.8),
                          elevation: 0.0,
                          child:  MaterialButton(
                            minWidth: MediaQuery.of(context).size.width,
                            child: Text('next ',
                                style:TextStyle(
                                    color: color_app.color_app_white,
                                    fontSize: 20.0
                                )),
                            onPressed:()=>redirectToPage3(context),

                          )
                      ),
                    ),
                        SizedBox(height: 14.0),

                      ],
                    )
                )
            ),
          ],
        ),

      ),
    );





  }
  Future<Null> _pickImageFromGallery() async{
    final File imageFile= await ImagePicker.pickImage(source: ImageSource.gallery);
    setState(() {
      this._imageFile=imageFile;
    });

  }
  Future<Null> _pickImageFromCamera() async{
    final File imageFile= await ImagePicker.pickImage(source: ImageSource.camera  );
    setState(() {
      this._imageFile=imageFile;
    });

  }
}
