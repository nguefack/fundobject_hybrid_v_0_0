import 'dart:convert';
import 'dart:io';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:fund_app/src/Models/Objekt.dart';
import 'package:path/path.dart';
import 'dart:convert';

class RepositoryObjekt{
  final dbs= FirebaseStorage.instance;
  final db=FirebaseDatabase.instance;
}
class Rufe_Objekten extends RepositoryObjekt {


  String name,image_name,name_object_Table='objekts',objects_rental='objects_rental';
  Future<String> save_file_firebase(File file) async{

    image_name=basename(file.path);
    StorageReference storageReference= dbs.ref().child(image_name);
    StorageUploadTask task= storageReference.putFile(file);
    var download_url_= await(await task.onComplete).ref.getDownloadURL();
    var url=download_url_.toString();
    return url;
  }

  Future<bool> save_objekt_(Objekt o){

    toJson(){
      return {
        "id_user":o.id_user,
        "name":o.name,
        "name_user":o.name_user,
        "action":o.action,
        "category":o.category,
        "description":o.description,
        "price":o.price,
        "image_object":o.image_object,
        "tel_nummer":o.tel_nummer,
        "plz":o.plz,
        "stadt":o.stadt,
        "strasse":o.strasse,
        "date":o.date,
        "image_name":o.image_name
      };
    }


    DatabaseReference dR = db.reference().child(name_object_Table);

    dR.push().set(toJson());

  }

  Future<Query> get_ObjectList_() async{

     return db.reference().child('objekts');

  }
  Future<List<Objekt>> get_ObjectList() async{
    List<Objekt> item_list=[];
      await db.reference().child('objekts').once().then((DataSnapshot snap){
      var keys= snap.value.keys;
      var data= snap.value;
      for(var key in keys){
        Objekt objekt= Objekt.fromMap(data[key]);

        item_list.add(objekt);
      }

      return null;
    });

    return item_list;

  }

  fromJson( String key,String json_object){
    Objekt objekt= new Objekt();
    if(json_object!=null) {
      objekt.name = json_object;
    }else{
      objekt.name='error_name server';
    }
    objekt.key=int.parse(key);
    return objekt;

  }

  Future<List<Objekt>> getObjectList() async{

    List<Objekt> ObjectList = new List<Objekt>();

    FirebaseDatabase.instance.reference().child(name_object_Table).once()
        .then((DataSnapshot snapshot){
          var object = fromJSON(snapshot.key, snapshot.value);
          ObjectList.add(object);
    }

    );

  }

  fromJSON(String key, String value){
    Objekt objekt_ = jsonDecode(value);
    objekt_.key = int.parse(key);
    return objekt_;
  }
   Future<bool> save_objekt_rental(String id_receiver,Objekt o){

     toJson(){
       return {
         "id_user":o.id_user,
         "id_receiver":id_receiver,
         "name":o.name,
         "name_user":o.name_user,
         "action":o.action,
         "category":o.category,
         "description":o.description,
         "price":o.price,
         "image_object":o.image_object,
         "tel_nummer":o.tel_nummer,
         "plz":o.plz,
         "stadt":o.stadt,
         "strasse":o.strasse,
         "date":o.date,
         "image_name":o.image_name
       };
     }


     DatabaseReference dR = db.reference().child(objects_rental);

     dR.push().set(toJson());

   }



  Future<List<Objekt>> get_ObjectList_favorite(String id_user) async{
    List<Objekt> item_list=[];
    await db.reference().child('objects_rental').once().then((DataSnapshot snap){
       if(snap.value!=null) {
        var keys = snap.value.keys;
        var data = snap.value;
        for (var key in keys) {
          if (id_user == data[key]['id_receiver']) {
            Objekt objekt = Objekt.fromMap(data[key]);
            item_list.add(objekt);
          }
        }
      }
      return null;
    });

    return item_list;

  }
  Future<List<Objekt>> get_ObjectList_publication(String id_user) async{
    List<Objekt> item_list=[];
    await db.reference().child('objekts').once().then((DataSnapshot snap){
      if(snap.value!=null) {
        var keys = snap.value.keys;
        var data = snap.value;
        for (var key in keys) {
          if (id_user == data[key]['id_user']) {
            Objekt objekt = Objekt.fromMap(data[key]);
            item_list.add(objekt);
          }
        }
      }
      return null;
    });

    return item_list;

  }


}