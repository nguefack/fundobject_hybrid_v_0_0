import 'dart:async';
import 'dart:convert';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:fund_app/src/Models/Objekt.dart';
import 'package:fund_app/src/Models/User.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Repository{
  final db = FirebaseDatabase.instance;
  final dbA = FirebaseAuth.instance;

}

class Registration extends Repository {

  Future<String> saveUserAuthenticationtable(final user_name,final user_email, final user_password, final user_telephoneNummer) async {

    UserServices userServices= new UserServices();
    String statusRequest="null";

    try{

      FirebaseUser user1 = await  dbA.createUserWithEmailAndPassword(email: user_email, password: user_password);
      print("cesaire test current user id");
      print("cesaire test"+user1?.uid+" current user id");
      User user = new User();
      user.userId=user1?.uid;
      user.userName=user_name;
      user.telefonNummer=user_telephoneNummer;
      save_User(user);
      userServices.saveCurentUserId(user1?.uid,user_name,user_email);

      statusRequest= "success";
    }catch(e){
      print(e.toString());
      if(e.toString()=="PlatformException(ERROR_EMAIL_ALREADY_IN_USE, The email address is already in use by another account., null)"){
        statusRequest= "ERROR EMAIL ALREADY IN USE";
      }else if(e.toString()=="PlatformException(ERROR_INVALID_EMAIL, The email address is badly formatted., null)"){
        statusRequest= "ERROR INVALID EMAIL";
      }else if(e.toString()=="PlatformException(ERROR_NETWORK_REQUEST_FAILED, A network error (such as timeout, interrupted connection or unreachable host) has occurred., null)"){
        statusRequest= "ERROR NETWORK checks your internet connection";
      }else{
        statusRequest=e.toString();
      }

    }
    return statusRequest;

  }

// ignore: non_constant_identifier_names
bool save_User(User u){
  toJson(){
    return{
      "username":u.userName,
      "telefonNummer":u.telefonNummer,
      "userId":u.userId,
      "my_objekts":u.my_objekts,
      "my_commandes":u.my_commandes,
      "profil":u.profil,
      "token_user":u.token_user

    };
  }
  try{
    DatabaseReference dR = db.reference().child(u.name_User_Table);

    dR.push().set(toJson());
    return true;
  }catch(e){
      return false;
  }

}

}

class Login  extends Repository{


  Future<String> getUser( final email,final password) async{
    FirebaseUser user = await dbA.signInWithEmailAndPassword(email: email, password: password);

    return user.uid;
  }

  // ignore: non_constant_identifier_names
  Future<String> Start_Session_user(final idUser,final emailUser) async{
    try{
      DatabaseReference  rf = db.reference().child("users") ;//.child(idUser);
      rf.once().then((DataSnapshot dataSnapshot){
        var keys= dataSnapshot.value.keys;
        var values=dataSnapshot.value;
        for(var key in keys){
          if(values[key]["userId"]==idUser){
            UserServices  userServices= new UserServices();
            userServices.saveCurentUserId(idUser,values[key]["username"],emailUser);

          }
        }
      });
      return "start session";
    }catch(e){
      return "start session error";
    }

  }

}

abstract class BaseUserService extends Repository{

  Future<String>createUser(String email, String password);
  Future<String>_currentUser ();
  // ignore: non_constant_identifier_names
  Future<Query> currentUser_info(String id);

}

class UserServices implements BaseUserService{
  // ignore: non_constant_identifier_names
  String name_User_Table = 'users';

  Future<String>createUser(String email, String password) async{
    FirebaseUser user = await FirebaseAuth.instance
        .signInWithEmailAndPassword(
        email: email, password: password);
    return user.uid;
  }

  Future<String>_currentUser () async{
    FirebaseUser user = await dbA.currentUser();
    return user?.uid;
  }



  Future<bool> saveCurentUserId(String id,String user_name,String user_email)async {
    SharedPreferences preference;
    preference = await SharedPreferences.getInstance();
    preference.setString("id_user", id);
    preference.setString("user_name", user_name);
    preference.setString("user_email", user_email);
    return preference.commit();
  }

  Future<String> getId_()async{
    SharedPreferences sharedPreferences=await SharedPreferences.getInstance();
    String id= sharedPreferences.getString("id_user");
    return id;
  }
  Future<String> getUserName()async{
    SharedPreferences sharedPreferences=await SharedPreferences.getInstance();
    String id= sharedPreferences.getString("user_name");
    return id;
  }
  Future<String> getUserEmail()async{
    SharedPreferences sharedPreferences=await SharedPreferences.getInstance();
    String id= sharedPreferences.getString("user_email");
    return id;
  }

  void logoutUser()async{
    // Clearing all data from Shared Preferences
    SharedPreferences sharedPreferences=await SharedPreferences.getInstance();
    sharedPreferences.clear();
    sharedPreferences.commit();
  }

  @override
  Future<Query> currentUser_info(String id_user) async{
    return FirebaseDatabase.instance.reference().child(name_User_Table);//.child(id_user);


  }

  Future<String> insert_id_object(String user_commande,String id_user,String id_object) async {

    Map<String, dynamic> map = jsonDecode(user_commande);
    String new_commandes="null";
    if(map["my_commandes"]=="null"){
      new_commandes="{id_object0:"+id_object+"}";
    }else{

      String new_my_objects="[";
      Map<String, dynamic> map_= json.decode(map["my_commandes"]);
      for(int i=0;i<map_.length;i++){
        String object_id=map_[i];
        //print(object_id["id_object0"]);
        // new_my_objects+=object_id+",";
      }
      //new_my_objects+="{id_object:"+id_object+"}]";
      //my_objects=new_my_objects;

    }

    toJson(){
      return {

        "my_commandes":"$new_commandes",
        "my_objekts":map["my_objekts"],
        "profil":map["profil"],
        "telefonNummer":map["telefonNummer"],
        "token_user":map["token_user"],
        "userId":map["userId"],
        "username":map["username"],
      };
    }


    DatabaseReference  rf =FirebaseDatabase.instance.reference().child(name_User_Table).child(id_user);
    await rf.update(toJson()).then((onValue){
      print("ok ich habe es angefangen");
    });
    return id_user;
  }

  @override
  // TODO: implement db
  FirebaseDatabase get db => null;

  @override
  // TODO: implement dbA
  FirebaseAuth get dbA => null;

}
