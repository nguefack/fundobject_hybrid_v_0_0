import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:fund_app/src/screens/widgets/themes_colors.dart';
import 'package:fund_app/src/screens/welcome_screen.dart';

void main() => runApp(Myapp());

class Myapp extends StatefulWidget  {
  @override
  State<StatefulWidget> createState() {
    return _MyApp();
  }
}

class _MyApp extends State<Myapp> {
  Color_app color_app= new Color_app();
  @override
  build(context){
    return MaterialApp(
      theme: new ThemeData(primarySwatch: Colors.lime,
          primaryColor: defaultTargetPlatform ==
              TargetPlatform.iOS ? color_app.color_app_def_val
              : color_app.color_app_def_val),
       debugShowCheckedModeBanner: false,
       home: WelcomeScreen(),
      //create a page
    );
  }
}
